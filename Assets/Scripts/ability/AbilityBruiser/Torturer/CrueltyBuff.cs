﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Cruelty Buff", menuName = "Ability/Bruiser/Torturer/Cruelty Buff")]
    public class CrueltyBuff : BuffDebuff {
        [SerializeField]
        BuffDebuff CrueltySadismBuff;
        [SerializeField]
        float crueltyDuration = 4f;

        public CrueltyBuff()
        {
            Name = "cruelty_buff";
            MaximumStacks = 5;
            OnApply = (pair, holder) =>
            {
                if (holder.StacksOf(pair) >= pair.ability.MaximumStacks)
                {
                    //set the base effect on cooldown and apply the new buff
                    pair.Source.StartCooldown(pair.PassiveParent);  // Is this right?

                    // wait what the fuck? Is this a buff or a targetted thing?
                    holder.AddBuff(CrueltySadismBuff.WithSource(pair.Source));
                    holder.RemoveBuff(pair, fullClear: true);
                }
            };
            AlsoObsolete_Duration = (pair) =>
            {
                return crueltyDuration;
            };
            /*
                Obsolete_OnApply = (Passive self, Unit source, Unit target) =>
                {
                    if (self.CurrentStacks >= self.MaximumStacks)
                    {
                        //set the base effect on cooldown and apply the new buff
                        self.Parent.StartCooldown(source);
                        
                        target.AddBuff(CrueltySadismBuff);
                        target.RemoveBuff(CrueltyBuff);
                    }
                },
              */
        }
    }
}