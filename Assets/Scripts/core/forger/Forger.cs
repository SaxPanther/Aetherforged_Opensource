using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using core;
using core.lane;
using core.collection;
using controllers;
using feature;
using managers;
using itemdata = feature.ItemLoader;

namespace core.forger {
    public class Forger : Unit {
        public override string Name {
            get {
                if (player != null) {
                    return player.PlayerName;
                }
                return unitName;
            }
        }

        public override UnitKind Kind {
            get { return UnitKind.Forger; }
        }

        readonly int MAX_LEVEL = 20;
        readonly float BONUS_PER_LEVEL = 0.25f;
        readonly float PENALTY_PER_LEVEL = 0.10f;
        readonly float MINIMUM_MULTIPLIER = 0.1f;

        public Dictionary<Forger, int> CurrStreaks = new Dictionary<Forger, int>();

        public List<float> MultiKillTimeStamps = new List<float>();

        public List<Forger> MultiKillForgers = new List<Forger>();
        public int KillSpree = 0;

        public override float SaltBounty {
            get {
                // TODO 2017 Jun 19 handle death streak. 
                return 300;
            }
        }

        public override BountyType Bounty {
            get {
                return BountyType.Assist;
            }
        }

        public override float GlobalSaltBounty {
            get {
                // TODO 2017 Jun 19 kill streak bounty. 
                return base.GlobalSaltBounty;
            }
        }

        public string PrettyKDA {
            get {
                return string.Format(
                    "{2:00}/{1:00}/{0:00}"
                    , Assists
                    , Deaths
                    , Kills
                );
            }
        }

        public float XPRatio {
            get { return currentExperience / ToNextLevel; }
        }

        public float Salt {
            get { return Mathf.Floor(currentSalt); }
        }

        public float SaltRatio {
            // Shouldn't this just be the inverse of the KDA ratio? :^)
            get { return Mathf.Min(currentSalt, SaltGoal) / SaltGoal; }
        }

        public float SaltGoal {
            // FIXME once we add the shopping list, we can use the cost to complete the first item off that.
            get {return 3000f;}
        }

        protected override void Initialize()
        {
            base.Initialize();
            currentSalt += Constants.STARTING_SALT;
        }

        public void SetName(string myName) 
        {
            unitName = myName;
        }

        public void SetPlayer(PlayerKnob player) {
            if (NetworkServer.active) {
                this.player = player;
            }
        }

        #region leveling and advancement
        // points to improve skills
        protected int skillPoint = 1;

        protected int unlockSpellPoint = 1;

        protected int upgradeSpellPoint;

        int MaxBasicSkillLevel
        {
            get
            {
                return Mathf.Min(5, Mathf.CeilToInt(Level / 2f));
            }
        }

        int MaxUltimateAbilityLevel
        {
            get
            {
                return Mathf.Min(3, Mathf.FloorToInt(Level / 6));
            }
        }

        public bool CanSkillUp(int slot) 
        {
            var gonnaBeLevel = LevelList[slot] + 1;

            return ((skillPoint > 0) &&
                    ((gonnaBeLevel <= MaxUltimateAbilityLevel) ||
                     (!SkillSlots[slot].IsUltimate && gonnaBeLevel <= MaxBasicSkillLevel)));
        }

        public bool SkillUp(int slot) {
            /*
            var gonnaBeLevel = LevelList[slot] +1;

            if (skillPoint <= 0)
            {
                return false;
            }
            if (SkillSlots[slot].IsUltimate && gonnaBeLevel > MaxUltimateAbilityLevel)
            {
                return false;
            }
            if (gonnaBeLevel > MaxBasicSkillLevel)
            {
                return false;
            }
            */

            if (CanSkillUp(slot))
            {
                --skillPoint;
                LevelList[slot] += 1;
                return true;
            }
            return false;
        }
        
        public void GainXP (float amount) {
            currentExperience += amount;

            if (Level < MAX_LEVEL && currentExperience >= ToNextLevel) {
                currentExperience -= ToNextLevel;

                ++Level;

                switch (Level)
                {
                    case 7:
                    case 13:
                        ++upgradeSpellPoint;
                        ++unlockSpellPoint;
                        Debug.Log("Gained spell point and upgrade spell");
                        break;
                    case 20:
                        ++upgradeSpellPoint;
                        ++skillPoint;
                        Debug.Log("Gained skill point and upgrade spell");
                        break;
                    default:
                        ++skillPoint;
                        Debug.Log("Gained skill point");
                        break;
                }


                RpcLevelUpDressings();
            }
        }

        public void GainKillXP (Forger forger) {
            float amount = ToNextLevel/2f;  // half the XP to get to next level from 0 exp

            float multiplier = 1f;

            // adjust the multiplier based on the distance above or below killer's current level
            multiplier += ((forger.Level - Level) * ((forger.Level >= Level) ? BONUS_PER_LEVEL : PENALTY_PER_LEVEL));

            if (multiplier < MINIMUM_MULTIPLIER) {
                // TODO 2017 Jun 19 add a joke debuff here. Sneak in for April 1st if not accepted during normal games.
                multiplier = MINIMUM_MULTIPLIER;
            }
            // add any adjustments to the XP amount here. 

            amount *= multiplier;

            // last warning. All aboard! :P
            GainXP(amount);
        }

        public void GainSalt (float amount, bool passive) {
            currentSalt += amount;
            totalSalt += amount;

            if (passive && amount > 0) {
                RpcSaltDressings(passive);
            }
        }

        public void QuietSaltGain (float amount) {
            currentSalt += amount;
            totalSalt += amount;
        }


        #endregion

        public override void Colourize()
        {
            var bars = transform.Find("ForgerCanvas").GetComponentsInChildren<Image>();

            if (modelReady)
            {
                Color colour;

                if (this == UnitManager.Instance.LocalForger) 
                {
                    colour = UnitManager.Instance.OwnForgerColour;
                }
                else 
                {
                    colour = UnitManager.Instance.AppropriateColour(Team);
                }



                foreach (var bar in bars)
                {
                    if (bar.name == "HealthBar")
                    {
                        bar.color = colour;
                    }
                }

            }
        }



        public bool BuyItem(Item buyThisItem) {
            // MEME consume obey stay asleep obey consume

            return _buyItem(buyThisItem);

        }

        public void SellItem(Item sellThisItem)
        {
            _sellItem(sellThisItem);
        }



        bool _buyItem(Item buyThisItem) {
            // MEME consume obey stay asleep obey consume

            float discount = 0f;
            int componentIndex = -1;

            if (buyThisItem.BuildsFrom.Length > 0) {
                List<Item> priorTier = new List<Item>();
                List<Item> traceLineage = new List<Item>();

                // Less expensive, but could probably be better.
                // Mostly way more readable which is a big plus.
                foreach(var part in buyThisItem.AllParts)
                {
                    if (items.Contains(part) && part.Cost > discount)
                    {
                        discount = part.Cost;
                        componentIndex = items.IndexOf(part);
                    }
                }
            }

            if (Salt < buyThisItem.Cost - discount) {
                return false;
            }

            if (discount > 0) {
                var itemComponent = items [componentIndex];
                items.RemoveAt(componentIndex);
                itemNames.RemoveAt(componentIndex);

                if (itemComponent.StaticItemStats != UnitStats.Zero) {
                    ServerStatsUpdate(itemComponent.StaticItemStats, true, false);
                }

                // FIXME percent item stats

                if (itemComponent.UniquePassive != null) {
                    RemovePassive(itemComponent.UniquePassive.WithSource(this));
                }

                if (itemComponent.StackablePassive != null) {
                    RemovePassive(itemComponent.StackablePassive.WithSource(this));
                }


            }




            if (items.Count < 6) {  
                // Add new item.
                items.Add(buyThisItem);
                itemNames.Add(buyThisItem.Name);
                currentSalt -= buyThisItem.Cost - discount;

                {
                    if (buyThisItem.StaticItemStats != null) {
                        ServerStatsUpdate(buyThisItem.StaticItemStats, true, true);
                    }
                    if (buyThisItem.PercentItemStats != null && buyThisItem.PercentItemStats != default(UnitStats)) {
                        ServerStatsUpdate(buyThisItem.PercentItemStats, false, true);
                    }
                    if (buyThisItem.UniquePassive != null) {
                        AddPassive(buyThisItem.UniquePassive, this);
                    }
                    if (buyThisItem.StackablePassive != null) {
                        AddPassive(buyThisItem.StackablePassive, this);
                    }
                }
                return true;
            }
            return false;
        }

        void _sellItem(Item sellThisItem) {
            if(itemNames.Contains(sellThisItem.Name))
            {
                items.Remove(sellThisItem);
                itemNames.Remove(sellThisItem.Name);
                currentSalt += (sellThisItem.Cost / 3);

                if (sellThisItem.StaticItemStats != UnitStats.Zero) 
                {
                    ServerStatsUpdate(sellThisItem.StaticItemStats, true, false);
                }
            }
        }

        [SyncVar]
        [SerializeField]
        protected float currentExperience;

        [SyncVar]
        [SerializeField]
        protected float currentSalt;

        protected float totalSalt;

        public float ToNextLevel {
            get { return 260 + 90 * Level; }
        }

        // (260 + 90 * x)
        // Did the math thing
        public float TotalExperience {
            // sum of (260 + 90x) where x is Level-1 and Level|1->20
            // then add progress toward next level
            get { return (45 * Level * Level + 215 * Level - 260) + currentExperience; }
        }

        /// <summary>
        /// The amount of total experience to get a level above my current level. 
        /// </summary>
        public float ExperienceLevelPlus {
            // sum of (260 + 90x) where x is Level and Level|1->20
            get {return Level * (305 + 45 * Level); }
        }

        PriorityAggroList MyAggroList = new PriorityAggroList();

        public PriorityAggroList MyAssistList = new PriorityAggroList();

        public override List<Forger> FullAggro {
            get {
                var time = GameTime.time;

                float cutoffTime = time - Constants.KILL_ASSIST_TIMEOUT;

                return MyAggroList.BetterThan(cutoffTime);
            }
        }

        public override void AddAggro (Forger forger)
        {
            base.AddAggro(forger);

            lastAggro = forger;

            var time = GameTime.time;

            float cutoffTime = time - Constants.KILL_ASSIST_TIMEOUT;

            // add any 
            foreach (var helper in forger.MyAssistList.BetterThan(cutoffTime)) {
                MyAggroList.Add( time, helper);
            }

            MyAggroList.Add( time , forger);
        }

        public int Kills {
            get;
            protected set;
        }

        public int Deaths {
            get;
            protected set;
        }

        public int Assists {
            get;
            protected set;
        }

        public int Creeps {
            get;
            protected set;
        }

        public void EarnKill () {
            Kills++;
        }

        public void EarnAssist () {
            Assists++;
        }

        public void EarnDeath () {
            Deaths++;
        }

        public void LastHitCreep () {
            ++Creeps;
        }

        public  PlayerKnob player {
            get;
            protected set;
        }

        Role role;
        public Role Role {
            get { return role; }
        }

        public List<Item> items = new List<Item>(7);

        public SyncListString itemNames = new SyncListString();

        internal override void Start() {
            base.Start();
            Level = 1;
        }

        protected override void AnnounceDeath(Unit Killer)
        {
            AnnouncerEvent currEvent = null;
            Forger myself = GetComponent<Forger>();

            if(Killer.Kind == UnitKind.Forger)
            {
                Forger killer = Killer.GetComponent<Forger>();
                currEvent = new AnnouncerEvent(AnnouncementType.Slain, "been slain", killer, myself);

                killer.KillSpree += 1;
                killer.MultiKillTimeStamps.Add(Mathf.Floor(GameTime.time));
                killer.MultiKillForgers.Add(myself);
                myself.KillSpree = 0;
                myself.MultiKillTimeStamps.Clear();
                myself.MultiKillForgers.Clear();
                myself.CurrStreaks[killer] = 0;
                killer.CurrStreaks[myself] += 1;

                if((Mathf.Floor(GameTime.time) - killer.MultiKillTimeStamps[(killer.MultiKillTimeStamps.Count-1)]) > 10)
                {
                    killer.MultiKillTimeStamps.Clear();
                    killer.MultiKillForgers.Clear();
                    killer.MultiKillTimeStamps.Add(Mathf.Floor(GameTime.time));
                    killer.MultiKillForgers.Add(myself);
                }
                else if(killer.MultiKillTimeStamps.Count >= 2)
                { 
                    switch(killer.MultiKillTimeStamps.Count)
                    {
                        case 2:
                            currEvent = new AnnouncerEvent(AnnouncementType.Double, "double kill", killer, killer.MultiKillForgers);
                            break;
                        case 3:
                            currEvent = new AnnouncerEvent(AnnouncementType.Triple, "triple kill", killer, killer.MultiKillForgers);
                            break;
                        case 4:
                            currEvent = new AnnouncerEvent(AnnouncementType.Quadra, "tetra kill", killer, killer.MultiKillForgers);
                            break;
                        case 5:
                            currEvent = new AnnouncerEvent(AnnouncementType.Penta, "omni kill", killer, killer.MultiKillForgers);
                            break;
                    }
                    AnnouncerController.Instance.QueueEvent(currEvent);
                }
                if(killer.KillSpree >= 3)
                {
                    switch(killer.KillSpree)
                    {
                        case 3:
                            currEvent = new AnnouncerEvent(AnnouncementType.KillSpree, "killing spree", killer, true);
                            break;
                    }
                }
                AnnouncerController.Instance.QueueEvent(currEvent);
                            
                if(killer.CurrStreaks[myself] >= 3)
                {
                    currEvent = new AnnouncerEvent(AnnouncementType.Dominating, "dominating", killer, myself);
                    AnnouncerController.Instance.QueueEvent(currEvent); 
                }
            }
            else if(Killer.Kind == UnitKind.Tower)
            {
                Tower killer = Killer.GetComponent<Tower>();
            }
            else if(Killer.Kind == UnitKind.Minion)
            {
                Minion killer = Killer.GetComponent<Minion>();
            }

            if(Killer.Kind == UnitKind.Minion || Killer.Kind == UnitKind.Tower || Killer.Kind == UnitKind.Monster)
            {
                currEvent = new AnnouncerEvent(AnnouncementType.Execute, "has been executed", Killer, myself);
                AnnouncerController.Instance.QueueEvent(currEvent);
            }

        }

        protected override void AnnounceDeath()
        {
            AnnouncerEvent currEvent = null;
            Forger myself = GetComponent<Forger>();

            currEvent = new AnnouncerEvent(AnnouncementType.Execute, "died mysteriously", myself, false);
            AnnouncerController.Instance.QueueEvent(currEvent);
        }


        [ClientRpc]
        void RpcLevelUpDressings() {


            // TODO redo this once we have a particle manager of some sort.
            GameObject levelUpParticle;
                levelUpParticle = (GameObject)Resources.Load("Particles/General/level_up/lvlup_effect 1");
                var levelupAudio = GetComponent<AudioSource>();
                // TODO redo this once we have an audio manager of some sort.
                levelupAudio.clip = (AudioClip)Resources.Load("Audio/SFX/Level Up less ten");
                levelupAudio.Play();
            var channelObject = Instantiate(levelUpParticle, transform.position, transform.rotation, channelRoot);
            channelObject.transform.localPosition = Vector3.zero;
        }
    }


    public class PriorityAggroList : MaxHeap<float, Forger> {

    }
}
