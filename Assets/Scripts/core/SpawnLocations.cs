﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core {
//    public class SpawnLocations {
//
//    	// Use this for initialization
//    	void Start () {
//    		
//    	}
//    	
//    	// Update is called once per frame
//    	void Update () {
//    		
//    	}
//    }


    [System.Serializable]
    public class SpawnLocation {
        #if UNITY_EDITOR
        // these help us design, but aren't really necessary for engine
        public string description;
        public Color color;
        public GameObject gogogo;
        #endif
        public Team team;
        public UnitKind kind;
        public char spec;
        public Vector2 spot;
        public Vector3 unity_spot {
            get {
                return new Vector3(spot.x, 0, spot.y) * Constants.TO_UNITY_UNITS;
            }
            set {
                spot = new Vector2(value.x, value.z) * Constants.TO_AETHERFORGED_UNITS;
            }
        }

    }


//    public enum SpawnLocation {
//        NeutralTeamLeech,
//        BlueTeamForger,
//        RedTeamForger,
//        BlueTeamMinion,
//        RedTeamMinion,
//    }

    public class SpawnSpec {
        public char spec;
        public Vector3 offset;

    }
}