using UnityEngine;
using System;

namespace core {
    public class DamagePacket {

        [Obsolete]
        public DamagePacket() {}

        public DamagePacket(Entity source, DamageActionType kind) {
            Kind = kind;
            if (source is Unit)
            {
                Source = source as Unit;
            }
            else
            {
                localUnitSource = null;
                MagicalPenetration = 0f;
                PhysicalPenetration = 0f;
                PercentMagicalPenetration = 0f;
                PercentPhysicalPenetration = 0f;
                PercentBonusCriticalDamage = 0f;
            }

            entitySource = source;
        }

        public DamagePacket(DamageActionType kind, Unit source, bool critical = false, bool singletarget = true) {
            Kind = kind;
            Source = source;
            IsOvercharged = critical;
            HasSingleTarget = singletarget;
        }

        private Entity entitySource;
        private Unit localUnitSource;
        public Unit Source {
            get {
                return localUnitSource;
            }
            protected set {
                localUnitSource = value;
                MagicalPenetration = localUnitSource.TotalStats.Penetration;
                PhysicalPenetration = localUnitSource.TotalStats.Penetration;
                PercentMagicalPenetration = localUnitSource.PercentPenetration;
                PercentPhysicalPenetration = localUnitSource.PercentPenetration;
                PercentBonusCriticalDamage = localUnitSource.PercentBonusCriticalDamage;
            }
        }
        public DamageActionType Kind;
        public float PhysicalDamage;
        public float MagicalDamage;
        public float PureDamage;

        public Unit PrimaryTarget;
        /// <summary>
        /// True if this packet is a critical hit.
        /// </summary>
        public bool IsOvercharged
        {
            get;
            protected set;
        }

        /// <summary>
        /// Indicates whether this instance has single target.
        /// </summary>
        /// <value><c>true</c> if this instance has single target; otherwise, <c>false</c>.</value>
        public bool HasSingleTarget
        {
            get;
            protected set;
        }

        // NOTE: not to be confused with Spell Overload

        public bool IsUnit {
            get {
                return localUnitSource != null;
            }
        }

        public float PercentPrePhysicalModifier;
        public float PercentPreMagicalModifier;

        public float PreMitigationPhysical
        {
            get;
            protected set;
        }
        public float PreMitigationMagical
        { 
            get;
            protected set;
        }
        private bool precalculated = false;
        private bool mitigated = false;

        public float PostMitigationPhysical;
        public float PostMitigationMagical;

        public float PhysicalPenetration;
        public float MagicalPenetration;
        public float PercentPhysicalPenetration;
        public float PercentMagicalPenetration;
        public float PercentBonusCriticalDamage;

        public bool IsPrimaryTarget
        {
            get;
            protected set;
        }

        [Obsolete]
        public float PercentPostDamageModifier;
        public float PercentPostPhysicalModifier;
        public float PercentPostMagicalModifier;
        public float FlatPhysicalDamageReduction;
        public float FlatMagicalDamageReduction;

        /// <summary>
        /// Resets the modifiers that are specific to damage targets.
        /// We generally only need to do this when closing out Passive/Buff triggers 
        /// that modify incoming damage. Especially in a multi-hit situation. 
        /// </summary>
        public void ResetTargetOrientedModifiers() {
            PercentPostDamageModifier = 0;
            FlatPhysicalDamageReduction = 0;
            FlatMagicalDamageReduction = 0;
            mitigated = false;
        }

        public void Precalculate() {
            if (IsOvercharged)
            {
                PreMitigationPhysical = PhysicalDamage * Percent(PercentPrePhysicalModifier)
                * ((200f + PercentBonusCriticalDamage) / 100f);
                PreMitigationMagical = MagicalDamage * Percent(PercentPreMagicalModifier)
                * ((200f + PercentBonusCriticalDamage) / 100f);
            }
            else
            {
                PreMitigationPhysical = PhysicalDamage * Percent(PercentPrePhysicalModifier);
                PreMitigationMagical = MagicalDamage * Percent(PercentPreMagicalModifier);
            }
            precalculated = true;
            mitigated = false;
        }

        public void CalculateForTarget(Unit targetUnit) {
            if (!precalculated)
                Precalculate();

            if (HasSingleTarget && PrimaryTarget == null)
            {
                PrimaryTarget = targetUnit;
            }

            IsPrimaryTarget = (PrimaryTarget == targetUnit);
            
            PostMitigationPhysical = (PreMitigationPhysical
            * DamageReduction(targetUnit.Armor * Percent(-PercentPhysicalPenetration) - PhysicalPenetration))
            * Percent(PercentPostPhysicalModifier) - FlatPhysicalDamageReduction;
            PostMitigationMagical = (PreMitigationMagical
            * DamageReduction(targetUnit.Resistance * Percent(-PercentMagicalPenetration) - MagicalPenetration))
            * Percent(PercentPostMagicalModifier) - FlatMagicalDamageReduction;
            
            mitigated = true;
        }

        public float TotalDamageDealtTo(Unit targetUnit) {
            if (!mitigated)
                CalculateForTarget(targetUnit);
            
            return TotalDamage;
        }

        public float TotalDamage {
            get {
                return Mathf.Max(0f, 
                    (PostMitigationPhysical + PostMitigationMagical + PureDamage));
            }
        }

        protected static float DamageReduction(float armorOrResist) {
            if (armorOrResist >= 0)
            {
                return (100f / (100f + armorOrResist));
            }
            else
            {
                return 1f;  // we are not league. less than zero armor does not exist. 
            }
        }

        public float SliceOfTotalDamageDealtTo(Unit targetUnit, float divisor) {
            return TotalDamageDealtTo(targetUnit) / divisor;
        }

        public static float Percent(float percent) {
            return (100f + percent) / 100f;
        }

    }

    public class DamageActionType
    {
        public static DamageActionType AutoAttack = new DamageActionType();
        public static DamageActionType SkillOrAbility = new DamageActionType();
        public static DamageActionType ItemEffect = new DamageActionType();
        public static DamageActionType SpellCast = new DamageActionType();
        public static DamageActionType SiegeEffect = new DamageActionType();
    }

}