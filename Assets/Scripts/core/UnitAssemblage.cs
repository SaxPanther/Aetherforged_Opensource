﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

using core;

/// <summary>
/// A gathering of Unit prefabs, kits, and models.
/// </summary>
[CreateAssetMenu(fileName = "Assemblage", menuName = "Units/Assemblage")]
public class UnitAssemblage : ScriptableObject {


    [SerializeField]
    public List<UnitModelTuple> forgerPrefabs;

    [SerializeField]
    public List<TeamSpecModelTuple> minionPrefabs;

    [SerializeField]
    public List<TeamSpecModelTuple> towerPrefabs;

    Dictionary<string,GameObject> forgers = new Dictionary<string, GameObject>();

    Dictionary<TeamSpecTuple, GameObject> minions = new Dictionary<TeamSpecTuple, GameObject>();

    Dictionary<TeamSpecTuple, GameObject> towers = new Dictionary<TeamSpecTuple, GameObject>();

    void OnEnable() {
        Debug.Log("Assemblage enabled now");

        foreach (UnitModelTuple tuple in forgerPrefabs)
        {
            forgers.Add(tuple.Name.ToLower(), tuple.Prefab);
        }

        foreach (TeamSpecModelTuple tuple in minionPrefabs)
        {
            var inTuple = new TeamSpecTuple() { spec = tuple.spec, team = tuple.team };
            minions.Add(inTuple, tuple.prefab);
        }

        foreach (TeamSpecModelTuple tuple in towerPrefabs)
        {
            var inTuple = new TeamSpecTuple() { spec = tuple.spec, team = tuple.team };
            towers.Add(inTuple, tuple.prefab);
        }
    }

    public GameObject GetPrefab(Unit unit) {
        switch (unit.Kind)
        {
            case UnitKind.Forger:
                if (forgers.ContainsKey(unit.KitName))
                {
                    return forgers[unit.KitName];
                }
                break;
            case UnitKind.Minion:
                {
                    var tuple = new TeamSpecTuple() { spec = unit.Spec, team = unit.Team };
                    if (minions.ContainsKey(tuple))
                    {
                        return minions[tuple];
                    }
                }
                break;
            case UnitKind.Tower:
                {
                    var tuple = new TeamSpecTuple() { spec = unit.Spec, team = unit.Team };
                    if (towers.ContainsKey(tuple))
                    {
                        return towers[tuple];
                    }
                }
                break;
            default:
                break;
        }
        return new GameObject("bork");
    }

    /// <summary>
    /// Gets a list of imprint names.
    /// </summary>
    /// <returns>The imprint list.</returns>
    public List<string> GetImprintList() {
        List<string> result = new List<string>();

        foreach (var kitName in forgers.Keys)
        {
            result.Add(kitName.ToTitleCase());
        }

        return result;
    }

}


[System.Serializable]
public struct UnitModelTuple {

    [SerializeField]
    public string Name;
    [SerializeField]
    public GameObject Prefab;
    // TODO variations are a thing. 
    // TODO once we have scriptable kits put that here
//    [SerializeField]
//    public GameObject Kit;

}

/// <summary>
/// A tuple for differentiating units that vary between team Team, with a spec too.
/// </summary>
[System.Serializable]
public struct TeamSpecTuple {

    [SerializeField]
    public char spec;
    [SerializeField]
    public Team team;

}

[System.Serializable]
public struct TeamSpecModelTuple {

    [SerializeField]
    public char spec;
    [SerializeField]
    public Team team;
    [SerializeField]
    public GameObject prefab;
}
    

#if UNITY_EDITOR
[CustomEditor(typeof(UnitAssemblage))]
public class AssemblageEditor : Editor {
    private ReorderableList forgerList;
    private ReorderableList minionList;
    private ReorderableList towerList;

    private void OnEnable() {
        forgerList = new ReorderableList(serializedObject, 
            serializedObject.FindProperty("forgerPrefabs"), 
            true, true, true, true);
        
        forgerList.drawHeaderCallback = (rect) => {
            EditorGUI.LabelField(rect, "Forger Prefabs");
        };

        forgerList.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = forgerList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            float spaceWidth = 5f;
            float dressWidth = 140f;
            float nameWidth = rect.width - dressWidth - spaceWidth;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Name"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + nameWidth + spaceWidth, rect.y, dressWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Prefab"), GUIContent.none);
        };

        minionList = new ReorderableList(serializedObject, 
            serializedObject.FindProperty("minionPrefabs"), 
            true, true, true, true);

        minionList.drawHeaderCallback = (rect) =>
        {
            EditorGUI.LabelField(rect, "Minion Prefabs");
        };

        minionList.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = minionList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            float spaceWidth = 5f;
            float teamWidth = 140f;
            float specWidth = 20f;
            float dressWidth = rect.width - teamWidth - specWidth - spaceWidth *2;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, teamWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("team"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + teamWidth + spaceWidth, rect.y, specWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("spec"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + teamWidth + specWidth + spaceWidth *2, rect.y, dressWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("prefab"), GUIContent.none);
        };

        towerList = new ReorderableList(serializedObject, 
            serializedObject.FindProperty("towerPrefabs"), 
            true, true, true, true);

        towerList.drawHeaderCallback = (rect) =>
            {
                EditorGUI.LabelField(rect, "Tower Prefabs");
            };

        towerList.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = towerList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            float spaceWidth = 5f;
            float teamWidth = 140f;
            float specWidth = 20f;
            float dressWidth = rect.width - teamWidth - specWidth - spaceWidth *2;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, teamWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("team"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + teamWidth + spaceWidth, rect.y, specWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("spec"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + teamWidth + specWidth + spaceWidth *2, rect.y, dressWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("prefab"), GUIContent.none);
        };
    }

    public override void OnInspectorGUI () {
        serializedObject.Update();

        forgerList.DoLayoutList();

        minionList.DoLayoutList();

        towerList.DoLayoutList();

//        EditorGUILayout.ObjectField(serializedObject.FindProperty("defaultDressing"));


        serializedObject.ApplyModifiedProperties();
    }

}
#endif