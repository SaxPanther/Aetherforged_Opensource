
namespace core {
    public static class Constants {
        public const float TO_AETHERFORGED_UNITS = 80f;
        public const float TO_UNITY_UNITS = 1 / 80f;
        public const float MELEE_DEFAULT_ATTACK_RANGE = 140f;
        public const float MIDRANGE_DEFAULT_ATTACK_RANGE = 350f;
        public const float RANGED_DEFAULT_ATTACK_RANGE = 550f;

        public const float BOUNTY_DISTRIBUTION_RANGE = 1300f;

        public const float KILL_ASSIST_TIMEOUT = 5f;
        public const float IN_COMBAT_TIMEOUT = 4f;

        public const float STARTING_SALT = 600f;
        // Note: this probably should be moved to a *by map* constant.
    }
}
