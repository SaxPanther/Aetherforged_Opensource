﻿using UnityEngine;
using System.Collections;

namespace core.utility
{
  public class DisableMe : MonoBehaviour 
  {
    public void DisableIt()
    {
      gameObject.SetActive( false );
    }
  }
}