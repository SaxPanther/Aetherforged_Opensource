﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace managers {
    public class CursorType
    {
        public static CursorType pointer = new CursorType(new Vector2(0, 0));
        public static CursorType select = new CursorType(new Vector2(0, 0));
        public static CursorType selectHighlighted = new CursorType(new Vector2(0, 0));

        public Vector2 hotspot; // This is the location that the click will occur at - such as the center of a crosshair
        private CursorType(Vector2 hotspot) { this.hotspot = hotspot; }
    }

    public class CursorManager : MonoBehaviour {

        private static CursorManager _instance;

        public static CursorManager Instance {
            get {
                return _instance;
            }
        }

        [SerializeField]
        private Texture2D cursorPointer;
        [SerializeField]
        private Texture2D cursorSelect;
        [SerializeField]
        private Texture2D cursorSelectHighlighted;
        [SerializeField]
        private Texture2D allyCursor;
        [SerializeField]
        private Texture2D enemyCursor;
        [SerializeField]
        private Texture2D pingCursor;

        public void Start()
        {
            _instance = this;
            #if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            // if we're running OSX manually scale the textures down. 
            {
                var temp_texture = new Texture2D(cursorPointer.width,cursorPointer.height,TextureFormat.ARGB32,false);
                temp_texture.SetPixels32(cursorPointer.GetPixels32());

                TextureScale.Bilinear(temp_texture,32,32);
                cursorPointer = temp_texture;
            }
            {
                var temp_texture = new Texture2D(cursorSelect.width,cursorSelect.height,TextureFormat.ARGB32,false);
                temp_texture.SetPixels32(cursorSelect.GetPixels32());

                TextureScale.Bilinear(temp_texture,32,32);
                cursorSelect = temp_texture;
            }
            {
                var temp_texture = new Texture2D(cursorSelectHighlighted.width,cursorSelectHighlighted.height,TextureFormat.ARGB32,false);
                temp_texture.SetPixels32(cursorSelectHighlighted.GetPixels32());

                TextureScale.Bilinear(temp_texture,32,32);
                cursorSelectHighlighted = temp_texture;
            }

            #endif
            SetCursor(CursorType.pointer);
        }

        public void SetCursor(CursorType cursorType, core.Unit target = null)
        {
            var cursorImage = cursorPointer;
            if (cursorType == CursorType.select)
            {
                cursorImage = cursorSelect;
            }
            if (cursorType == CursorType.selectHighlighted)
            {
                if (target != null) {
                    if (UnitManager.Instance.HomeTeam == target.Team) {
                        cursorImage = allyCursor;
                    } else {
                        cursorImage = enemyCursor;
                    }
                } else {
                    cursorImage = cursorSelectHighlighted;
                }
            }
//            #if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
//            Cursor.SetCursor(cursorImage, new Vector2(cursorType.hotspot.x * cursorImage.width, cursorType.hotspot.y * cursorImage.height), CursorMode.ForceSoftware);
//            #else
            Cursor.SetCursor(cursorImage, new Vector2(cursorType.hotspot.x * cursorImage.width, cursorType.hotspot.y * cursorImage.height), CursorMode.Auto);
//            #endif
        }   
    }
                    }