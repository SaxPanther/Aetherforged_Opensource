using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using core;

namespace controllers {
    public class KnockbackController : NetworkBehaviour {
        public List<KnockbackInstance> knockbacks = new List<KnockbackInstance>();

        public bool IsKnockedBack {
            get {
                return knockbacks.Count > 0;
            }
        }

        public void AddKnockback(Vector2 position, float duration, int distance, int height) {
            var knockback = new KnockbackInstance() {
                Direction = (GetComponent<Unit>().Position - position).normalized,
                Duration = duration,
                Distance = distance,
                Height = height
            };
            knockbacks.Add(knockback);
            new Timer().Start(() => {
                knockbacks.Remove(knockback);
                return 0f;
            }, duration);
        }

        public void Start() {

        }
        public void Update() {
            var dt = Time.deltaTime;
            foreach(KnockbackInstance knockback in knockbacks) {
                transform.position += new Vector3(knockback.Direction.x, 0, knockback.Direction.y) * dt * (knockback.Distance * Constants.TO_UNITY_UNITS)/ knockback.Duration;
                knockback.DurationSpent += dt;
            }
        }
    }

    public class KnockbackInstance {
        public Vector2 Direction;
        public float Duration;
        public int Distance;
        public int Height;
        public float DurationSpent;
    }
}