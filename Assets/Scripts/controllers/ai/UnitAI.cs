using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

using core;
using core.utility;
using core.collection;

namespace controllers.ai {
    public class UnitAI<T> : MonoBehaviour where T : Unit {

        protected const float CLOSE_ENOUGH_RANGE = 40f;
        protected const float AGGRO_RANGE = 500f;
        protected const float RECONSIDER_FREQUENCY = 3f;
        protected const float INTERACTION_FREQUENCY = 1.0f;

        [SerializeField]
        protected T me;

        [SerializeField]
        protected ActionState currentAction = ActionState.Idle;
        public ActionState CurrentAction {
            get { return currentAction; }
        }

        [SerializeField]
        protected Unit currentTargetUnit {
            get {
                return nextTarget.Top();
            }
        }

        protected float nextInteractionTime = 0f;

        [SerializeField]
        protected Sight eyes = new Sight();

        protected Stack<PriorityUnit> waitList = new Stack<PriorityUnit>();
        [SerializeField]
        protected PriorityUnitHeap nextTarget = new PriorityUnitHeap();
        [SerializeField]
        protected int currentPriority;

        [SerializeField]
        protected HashSet<Unit> buddies = new HashSet<Unit>();

        protected bool isAttackMoving = false;
        protected Vector3 AttackArea;


        float BOXCUTOFF = 8f; // One World Unit?
        [SerializeField]
        protected NavMeshAgent agent;
        protected SteeringAgent steerAgent;
        public Vector3 Destination {
            set {
                if (!getAgent) {

                    return;
                }

                if (agent.isOnNavMesh && agent.destination != value) {
                    agent.destination = value;
                    agent.isStopped = false;
                }
            }
            get {
                if (!getAgent) {
                    return transform.position;
                }

                return agent.destination;
            }
        }
        private bool getAgent {
            get {
                if (agent == null) {
                    agent = GetComponent<NavMeshAgent>();
                }


                if (agent == null) {
                    return false;
                }

                return true;
            }
        }

        private bool getSteerAgent
        {
            get
            {
                if (steerAgent == null)
                {
                    steerAgent = GetComponent<SteeringAgent>();
                }



                if (steerAgent == null)
                {
                    return false;
                }

                if (getAgent && steerAgent.agent == null)
                {
                    steerAgent.agent = agent;
                }

                return true;
            }
        }

        private bool bodyIsReady {
            get {
                if (me == null)
                {
                    me = GetComponent<T>();
                }
                return (me != null);
            }
        }
        
        public bool isDashing;

        public bool ReachedDestination {
            get { return bodyIsReady && me.IsInRange(Destination,BOXCUTOFF); }
        }

        [SerializeField]
        private Unit targetUnitToAttack;
        public bool HasTargetUnit {
            get { 
                return targetUnitToAttack != null && targetUnitToAttack.isActiveAndEnabled;
            }
        }
        public Unit TargetUnit {
            get { return targetUnitToAttack; }
        }

        // Use this for initialization
        void Start () {
            //            me = GetComponentInChildren<T>();
            me = GetComponent<T>();
            currentAction = ActionState.Idle;
            if (!getAgent) {
                // FIXME why did I think this was a thing to do?
//                enabled = false;
            }
            BootUp();
        }

        void Update () {
            if (!NetworkServer.active) {
                return;
            }

            while (nextTarget.Count > 0 && ( currentTargetUnit == null || currentTargetUnit.IsDying)) {
                nextTarget.Pop();
            }

            if (getAgent && getSteerAgent)
            {
                steerAgent.moveSpeed = me.MovementSpeed * Constants.TO_UNITY_UNITS;
                agent.speed = 0f;
                agent.enabled = me.CanMove;
                agent.autoRepath = false;
            }
            else if (getAgent && bodyIsReady)
            {
                agent.speed = me.MovementSpeed * Constants.TO_UNITY_UNITS;
                agent.enabled = me.CanMove && !me.IsDead;
            }
            else if (getSteerAgent && bodyIsReady)
            {
                steerAgent.moveSpeed = me.MovementSpeed * Constants.TO_UNITY_UNITS;
            }

            if(me.GetComponent<DashController>() != null)
            {
                isDashing = true;
                currentAction = ActionState.Dashing;
            }
            else
            {
                isDashing = false;
            }

            if (nextInteractionTime < GameTime.time)
            {
                GetInteractionSphere();
            }

            DoBrain();
            if (bodyIsReady && me.CanControl) {

                switch (currentAction) {
                case ActionState.AttackMove:
                    AttackMoveAlgor();
                    break;
                case ActionState.HoldGround:
                    HoldGroundAlgor();
                    break;
                case ActionState.Idle:
                    DoIdle();
                    break;
                case ActionState.Move:

                    if(getSteerAgent && bodyIsReady && !steerAgent.moving)
                    {
                        steerAgent.moving = true;
                    }
                        
                    if (getAgent && agent.enabled && bodyIsReady && me.IsInRange(Destination, CLOSE_ENOUGH_RANGE)) {
                        // We arrived. 
                        currentAction = ActionState.Idle;
                    }
                    break;
                case ActionState.Dashing:
                    if(!isDashing)
                    {
                        currentAction = ActionState.Idle;
                    }
                    break;
                }
            }


            // UnitAI is best home for this probably. 
            if (bodyIsReady && me.CanControl && HasTargetUnit) { 
                //If we have a target enemy unit, check range and stuff

                if (me.IsInRange( targetUnitToAttack, me.AttackRange )) {
                    //If we're in (Melee) basic attack range, melee attack

                    // TODO 2017 Apr 16 Revisit
                    Destination = transform.position; // stop moving cause we're attacking. 
                    if (me.AttackRange > Constants.MELEE_DEFAULT_ATTACK_RANGE) {
                        me.ProjectileAttack(targetUnitToAttack);
                    } else {
                        me.MeleeAttack(targetUnitToAttack);
                    }
                } else {
                    // if not, we move to the target unit. 
                    Destination = Utils.NearbyPoint3(transform.position, targetUnitToAttack.transform.position, me.AttackRange);
                    /*
                    Destination = targetUnitToAttack.transform.position;
                    */
                }
            }


        }

        public void OnDisable() {
            if (getAgent) {
                agent.enabled = false;
            }
        }

        protected virtual void BootUp () {}

        protected virtual void DoBrain () {
        }

        protected virtual void DoIdle () {
        }

        protected virtual int GetEnemyPriority (Unit unit) {
            return Mathf.CeilToInt(me.GetDistance(unit));
        }

        public void AttackAggressor( Unit caller, Unit unitTarget ) {
            switch (caller.Kind) {
            case UnitKind.Forger:
                if (unitTarget.Kind == UnitKind.Forger) {
                    AddTarget(1, unitTarget);
                } else if (unitTarget.Kind == UnitKind.Minion) {
                    AddTarget(2, unitTarget);     
                }
                break;

            case UnitKind.Minion:
                if (unitTarget.Kind == UnitKind.Minion) {
                    AddTarget(3, unitTarget);

                } else if (unitTarget.Kind == UnitKind.Tower) {
                    AddTarget(4, unitTarget);
                } else if (unitTarget.Kind == UnitKind.Forger) {
                    AddTarget(5, unitTarget);
                }
                break;
            }
        }

        public void AttackMove (Vector3 area) {
            if(currentAction == ActionState.Dashing)
                return;            
            currentAction = ActionState.AttackMove;
            isAttackMoving = true;
            AttackArea = area;
            agent.isStopped = false;

            Retarget();
        }

        public void DoNotAttackMove () {
            if (currentAction == ActionState.AttackMove) {
                currentAction = ActionState.Idle;
            }
        }

        public void JustMove (Vector3 spot) {
            if(currentAction == ActionState.Dashing)
                return;
            currentAction = ActionState.Move;
            if(agent.isOnNavMesh)
            {
                if (agent.isStopped)
                {
                    agent.isStopped = false;
                }
            }
            Destination = spot;
            targetUnitToAttack = null;
            nextTarget.Clear();
        }

        public bool SetAttackTarget (Unit target) {
            if (target.Team != me.Team) {
                targetUnitToAttack = target;
                return true;
            }
            return false;
        }

        protected Unit TakeNextTarget() {
            //            nextTarget.Pop();
            currentPriority = nextTarget.TopRank();
            //            currentTargetUnit = nextTarget.Top();
            return currentTargetUnit;
        }

        protected bool AddTarget(int priority, Unit unit) {
            if (unit.IsDead)
            {
                return false;
            }
            return nextTarget.Add(priority, unit);
        }

        protected void Retarget () {
            
            List<Unit> considerThese = new List<Unit>();

            considerThese.AddRange(nextTarget.Values);

            nextTarget.Clear();

            foreach (var unit in considerThese) {
                if (unit != null) {
                    AddTarget(GetEnemyPriority(unit), unit);
                }
            }
            if (HasTargetUnit) {
                AddTarget(Mathf.RoundToInt(Vector2.Distance(targetUnitToAttack.Position, me.Position)), targetUnitToAttack);
                DropAggro();
            }
        }

        protected void SetTarget(int priority, Unit unit) {
            SetAttackTarget(unit);
            currentPriority = priority;
        }

        protected void GetInteractionSphere() {
            nextInteractionTime = GameTime.time + INTERACTION_FREQUENCY;
            Vector3 orig = me.transform.position;
            var length = AGGRO_RANGE;
            
            List<Unit> inMe = new List<Unit>();

            foreach(var unit in Unit.ListOf.Where(((Unit arg) => {
                
                Vector2 loc = Utils.PlanarPoint(arg.gameObject.transform.position);
                Collider[] hits = Physics.OverlapSphere(orig, length * Constants.TO_UNITY_UNITS);
                return (hits.Contains(arg.GetComponent<Collider>()));
            })))
            {
                if (me == null || unit == null) {
                    return;
                }

                if (me.TeamQualifiesAs(TeamQualifier.EnemyTeam, unit.Team))
                {
                    eyes.Enemies.Add(unit);

                    AddTarget(GetEnemyPriority(unit), unit);

                }
                else if (me.TeamQualifiesAs(TeamQualifier.FriendlyTeam, unit.Team) && unit.Kind == UnitKind.Minion)
                {
                    if (!buddies.Contains(unit))
                    {
                        buddies.Add(unit);
                    }
                }
            }

        }

        protected void AttackMoveAlgor () {
            if (HasTargetUnit) {
                // if we have a target, 
                if (TargetUnit.IsDying || !me.IsInRange(TargetUnit, AGGRO_RANGE)) {
                    // if we're out out of aggro range, drop aggro.
                    DropAggro();
                }
            } else if (nextTarget.Count > 0) {
                // if we (don't have a target unit) but we saw a potential target before, try attacking that one.


                SetAttackTarget(TakeNextTarget());
            } else if (me.IsInRange(AttackArea, CLOSE_ENOUGH_RANGE)) {
                // We arrived. 
                DoNotAttackMove();
            }
            else {
                // 
                Vector3 point = AttackArea;
                Destination = point;

            }
        }

        protected void HoldGroundAlgor () {
            if (HasTargetUnit) {
                // if we have a target, check the validity of the target.
                if (TargetUnit.IsDying || !me.IsInRange(TargetUnit, me.AttackRange)) {
                    // if we're out out of range, drop aggro.
                    DropAggro();
                }
            } else if (nextTarget.Count > 0) {
                // if we (don't have a target unit) but we saw a potential target before, try attacking that one.
                SetAttackTarget(TakeNextTarget());
            } else {
                // Stay in HoldGround
            }
        }


        protected void DropAggro() {
            /*
            if (nextTarget.Count > 0) {
                nextTarget.Pop();   
            }
            */
            /*
            currentTargetUnit = null;
            */
            CancelAttackTarget();
        }

        bool RemoveTarget(Unit target) {
            /*
            if (me.TargetUnit == target) {
                DropAggro();
            }
            */

            return nextTarget.Remove(target);


        }


        public void CancelAttackTarget () {
            targetUnitToAttack = null;
        }

//        void OnTriggerEnter (Collider bonk) {
//            var unit = bonk.GetComponent<Unit>();
//
//            if (me == null || unit == null) {
//                return;
//            }
//
//            if (me.TeamQualifiesAs(TeamQualifier.EnemyTeam, unit.Team)) {
//                eyes.Enemies.Add(unit);
//
//                AddTarget(GetEnemyPriority(unit), unit);
//
//            } else if (me.TeamQualifiesAs( TeamQualifier.FriendlyTeam, unit.Team) && unit.Kind == UnitKind.Minion) {
//                if (!buddies.Contains(unit)) {
//                    buddies.Add(unit);
//                }
//            }
//        }
//
//        void OnTriggerExit(Collider bonk) {
//
//            Unit unit = bonk.GetComponent<Unit>();
//            eyes.Enemies.Remove(unit);
//
//            if (isActiveAndEnabled && unit != null) {
//
//
//                // for some reason this is triggering CONSTANTLY
//                RemoveTarget(unit);
//                // This condition lowers the output to a reasonable number. 
//                //                if (nextTarget.Count > 0) {
////                Debug.LogFormat("Name = {0} \n{1}", gameObject.name, nextTarget);
//                //                }
//            }
//        }

    }
    [System.Serializable]
    public enum ActionState {
        Idle,
        Other,
        AttackMove,
        Guard,
        HoldGround,
        Move,
        Dashing,
    }

    [System.Serializable]
    public class Sight {
        public List<Unit> Enemies = new List<Unit>();
    }

    [System.Serializable]
    public class PriorityUnitHeap : MinHeap<int, Unit> {

    }

    [System.Serializable]
    public class PriorityUnit {
        public int rank;
        public Unit unit;
    }
}
