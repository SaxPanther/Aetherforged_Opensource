using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using core;

namespace controllers
{
    public class OtherProjectileController : ProjectileController
    {
        public object Target; //Vector3, Vector2 (Velocity), Unit, Transform, GameObject


        public bool StopOnTarget = true;

        public Action StopOnTargetAction;

        public Action<Vector3> OnStop;

        public bool StopOnDuration = false;
        public float MaxDuration;
        public float Duration;

        public bool StopOnTerrain = false;
        private UnityEngine.Object ScheduledDestroy;




        public void Update()
        {
            if (Target == null)
            {
                StopProjectile();
                return;
            }

            var targetLocation = getTargetLocation();
            var deltaPosition = (targetLocation - transform.position).normalized * Velocity * Time.deltaTime;
            transform.position += deltaPosition;

            var stopDistance = 1f;

            if(GetComponent<Unit>() != null)
            {
                stopDistance = Constants.MELEE_DEFAULT_ATTACK_RANGE * Constants.TO_UNITY_UNITS;
            }

            if(StopOnTarget && (transform.position - targetLocation).magnitude < stopDistance)
            {
                StopProjectile();
                if(StopOnTargetAction != null)
                {
                    StopOnTargetAction();
                }
            }
        }


        void OnCollisionEnter(Collision bonk) {
            if (StopOnTerrain && bonk.gameObject.layer == LayerMask.NameToLayer("Terrain")) {
                StopProjectile();
            }

        }

//        void OnTriggerEnter(Collider other)
//        {
//            var thing = other;
//
//            if(other.gameObject.layer == LayerMask.NameToLayer("Terrain") && StopOnTerrain)
//            {
//                StopProjectile();
//            }
//        }
//
        
        void StopProjectile()
        {
            OnStop(transform.position);
            Destroy(this);
        }

        public Vector3 getTargetLocation()
        {
            if (Target.GetType() == typeof(Vector3)) return (Vector3)Target;
            if (Target.GetType() == typeof(Vector2)) return transform.position + new Vector3(((Vector2)Target).x, 0, ((Vector2)Target).y);

            if (Target.GetType() == typeof(GameObject)) return ((GameObject)Target).transform.position;
            if (Target.GetType() == typeof(Transform)) return ((Transform)Target).position;
            if (Target.GetType() == typeof(Unit)) return ((Unit)Target).transform.position;

            return transform.position;
        }
    }
}