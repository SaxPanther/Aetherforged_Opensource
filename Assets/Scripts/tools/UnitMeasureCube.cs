﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;


[ExecuteInEditMode]
public class UnitMeasureCube : MonoBehaviour {
    public float InAetherForgedUnits = 100f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale = Vector3.one * Constants.TO_UNITY_UNITS * InAetherForgedUnits;
	}
}
