﻿using UnityEngine;
using System.Collections;

public class ApplyGreyScale : MonoBehaviour {
	public Shader greyscale;
	public Shader standard;
	private Transform searcharea;

	[SerializeField]
	Transform modelRoot;
	[SerializeField]
	SkinnedMeshRenderer[] partsToApply;
	bool isOn;

	public bool GreyOn;


	// Use this for initialization
	void Start () {
		if(modelRoot == null){
			Transform[] searcharea = gameObject.transform.parent.GetComponentsInChildren<Transform>();
			foreach(Transform child in searcharea){
				if (child.name.Contains ("Model")) {
					modelRoot = child;
				}
			}
		}

		if (modelRoot != null)
			partsToApply = modelRoot.GetComponentsInChildren<SkinnedMeshRenderer>();
		// Note: used in SetTargetModel. May be split off if differentiation is needed. 
	}
	
	// Update is called once per frame
	void Update () {
		if (GreyOn != isOn) {
			foreach (SkinnedMeshRenderer rend in partsToApply) {
				if (rend.materials.Length == 1) {
					if (GreyOn) {
						rend.material.shader = greyscale;
					} else {
						rend.material.shader = standard;
					}
				} else {
					foreach (Material mat in rend.materials) {
						if (GreyOn) {
							mat.shader = greyscale;
						} else {
							mat.shader = standard;
						}
					}
				}
			}
			isOn = GreyOn;
		}
	}

	public bool SetTargetModel(GameObject model) {
		if (model != null) {
			modelRoot = model.transform;
			Start();
			return true;
		}
		return false;
	}

	// A couple accessors in case that's useful
	public void TurnOn () {
		GreyOn = true;
	}
	public void TurnOff () {
		GreyOn = false;
	}
}
