﻿using UnityEngine;
using core;
using feature.map;
using managers;

namespace graphics
{
    public class WellCaptureAnimation : MonoBehaviour
    {
        public Renderer ringRenderer;
        public Renderer indicatorRenderer;
        public MineCapture wellCapture;

        void Awake () {
            if (wellCapture == null) {
                wellCapture = GetComponent<MineCapture>();
            }
        }

        void Update () 
        {
            indicatorRenderer.material.SetColor("_Color", UnitManager.Instance.AppropriateColour(wellCapture.Current));
            if (wellCapture.IsLocked) {
                ringRenderer.material.SetColor("_Color", UnitManager.Instance.AppropriateColour(Team.Neutral));

                var textureOffset = new Vector2(0, -2f * wellCapture.LockTimeRatio + 0.5f);
                ringRenderer.material.SetTextureOffset("_MainTex", textureOffset);

            } else {


                ringRenderer.material.SetColor("_Color", UnitManager.Instance.AppropriateColour(wellCapture.Current.Opposite()));

                var textureOffset = new Vector2(0, -wellCapture.CaptureProgress + 0.5f);
                ringRenderer.material.SetTextureOffset("_MainTex", textureOffset);
            }
        }
    }
}