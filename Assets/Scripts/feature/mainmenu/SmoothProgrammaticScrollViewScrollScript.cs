﻿using UnityEngine;
using UnityEngine.UI;

public class SmoothProgrammaticScrollViewScrollScript : MonoBehaviour {

    public int Steps = 10;
    public int CurrentStep = 0;

    private float visibleStep = 0;
    private float startTime;

    public void SetVisibleStep(int step) {
        CurrentStep = step;
        startTime = Time.time;
    }

	// Use this for initialization
	void Start () {
        visibleStep = CurrentStep;
	}
	
	// Update is called once per frame
	void Update () {
        visibleStep = Mathf.Lerp(visibleStep, CurrentStep, (Time.time - startTime) * 1f);
        GetComponent<ScrollRect>().horizontalNormalizedPosition = visibleStep / (Steps - 1); //This is so Steps can be the actual count of the panels
	}
}
