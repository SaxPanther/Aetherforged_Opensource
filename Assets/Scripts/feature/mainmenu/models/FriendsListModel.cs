﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace feature.mainmenu.models {
    public class Friend {
        public string Username;
        public string Status;
    }
    public class FriendsListModel {
        public Friend[] Friends;
        public Friend[] SentFriendRequests;
        public Friend[] RecievedFriendRequests;
    }
}
