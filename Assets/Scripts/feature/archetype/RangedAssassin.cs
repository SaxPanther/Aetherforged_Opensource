﻿using core;
using core.forger;

namespace feature.kit
{
    public class RangedAssassin : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 505,
                    HealthRegen: 1.4f,
                    MovementSpeed: 385,
                    AutoAttackDamage: 56,
                    BaseAttackTime: 1.5f,
                    BaseAttackRange: Constants.MIDRANGE_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 74,
                    HealthRegen: .12f,
                    AutoAttackDamage: 2.7f,
                    AttackSpeed: 2.5f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return .25f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .35f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .5f;
            }
        }

    }
}