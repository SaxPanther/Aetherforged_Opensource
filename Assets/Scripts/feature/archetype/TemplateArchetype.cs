﻿using core;
using core.forger;

namespace feature.kit {
    public class TemplateArchetype : Obsolete_Archetype {
        public override UnitStats BaseStats {
            get {
                return UnitStats.Stats(
                    MaxHealth: 1,
                    HealthRegen: 1,
                    MovementSpeed: 1,
                    AutoAttackDamage: 1,
                    BaseAttackTime: 1,
                    BaseAttackRange: Constants.MELEE_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats {
            get {   
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.
                
                return UnitStats.Stats(
                    MaxHealth: 1,
                    HealthRegen: 1,
                    AutoAttackDamage: 1,
                    AttackSpeed: 1
                );
            }
        }

        public override float basicAttackDamagePerPower {
            get {
                return 1f;
            }
        }

        public override float attackSpeedPerHaste {
            get {
                return 1f;
            }
        }

        public override float cooldownReductionPerHaste {
            get {
                return 1f;
            }
        }
            
    }
}
