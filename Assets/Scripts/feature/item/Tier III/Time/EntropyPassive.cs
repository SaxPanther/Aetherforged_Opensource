﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Entropy", menuName = "Passives/Items/Entropy")]
    public class EntropyPassive : Passive
    {
        public float EntropyBuffChargeCooldown = 5;
        public float EntropySpeedBuffAmount = 60;
        public float EntropySpeedBuffDuration = .75f;
        public float EntropySlowAmount = 60;
        public float EntropySlowDuration = .75f;

        [SerializeField] BuffDebuff EntropyBuff;
        [SerializeField] BuffDebuff EntropyBuffTick;
        //TODO: This speed buff might need to be a status instead?
        [SerializeField] BuffDebuff EntropySpeedBuff;
        [SerializeField] Status EntropySlow;

        public EntropyPassive()
        {
            Name = "Entropy passive";
            OnApply = (SourcePair pair, Unit owner) =>
            {
                owner.AddBuff(EntropyBuff.WithSource(pair.Source), EntropyBuff.MaximumStacks);
            };
            OnRemove = (SourcePair pair, Unit owner) =>
            {
                owner.RemoveBuff(EntropyBuffTick.WithSource(pair.Source), fullClear: true);
                owner.RemoveBuff(EntropyBuff.WithSource(pair.Source), fullClear: true);
            };
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (owner.HasBuff(EntropyBuff.WithSource(pair.Source)))
                {
                    // fullClear: true removes every stack at once.
                    owner.RemoveBuff(EntropyBuff.WithSource(pair.Source) /*, fullClear: true*/);
                    //TODO: Need to make sure the entropy speed buff is set up right
                    owner.AddBuff(EntropySpeedBuff.WithSource(pair.Source));
                    target.AddBuff(EntropySlow.WithSource(pair.Source));
                    //IDEA: If a more *burst* concept is desired, add a buff stack for each and fullClear
                }
                return packet;
            };
            OnTick = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTime) =>
            {
                if (!owner.HasBuff(EntropyBuffTick.WithSource(pair.Source)))
                {
                    owner.AddBuff(EntropyBuffTick.WithSource(pair.Source));
                    owner.AddBuff(EntropyBuff.WithSource(pair.Source));
                }
            };
        }
    }
}