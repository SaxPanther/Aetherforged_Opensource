﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Prophecy", menuName = "Passives/Items/Prophecy")]
    public class ProphecyPassive : Passive
    {
        [SerializeField] float ProphecyImmunityDuration = 5;
        [SerializeField] float ProphecyRangedSlowPercent = .25f;
        [SerializeField] float ProphecyMeleeSlowPercent = .4f;
        [SerializeField] float ProphecySingleTargetSlowPercent = .35f;
        [SerializeField] float ProphecyAoESlowPercent = .15f;
        [SerializeField] float ProphecyMaxSlowPercent = .7f;
        [SerializeField] float ProphecyBuildingSlowDuration = .75f;
        [SerializeField] float ProphecyFinalSlowDuration = .25f;

        [SerializeField]
        BuffDebuff ProphecyImmunity;
        [SerializeField]
        Status ProphecyRangedSlow;
        [SerializeField]
        Status ProphecyMeleeSlow;
        [SerializeField]
        Status ProphecySingleTargetSlow;
        [SerializeField]
        Status ProphecyAoESlow;

        public ProphecyPassive()
        {
            Name = "Prophecy passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (!target.HasBuff(ProphecyImmunity.WithSource(pair.Source)) || target.Kind != UnitKind.Forger)
                {
                    if (packet.Kind == DamageActionType.AutoAttack || packet.Kind == DamageActionType.SkillOrAbility)
                    {
                        if (packet.Kind == DamageActionType.AutoAttack)
                        {
                            if (owner.AttackRange < Constants.MIDRANGE_DEFAULT_ATTACK_RANGE)
                            {
                                target.ApplySlow(ProphecyRangedSlow);
                            }
                            else
                            {
                                target.ApplySlow(ProphecyMeleeSlow);
                            }
                        }
                        else
                        {
                            if (packet.PrimaryTarget == target)
                            {
                                target.ApplySlow(ProphecyAoESlow);
                            }
                            else
                            {
                                target.ApplySlow(ProphecySingleTargetSlow);
                            }
                        }
                        target.AddBuff(ProphecyImmunity.WithSource(pair.Source));
                    }
                }
                return packet;
            };
        }
    }
}