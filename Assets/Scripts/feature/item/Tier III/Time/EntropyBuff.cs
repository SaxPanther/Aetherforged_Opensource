﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Entropy Buff", menuName = "Passives/Items/Entropy Buff")]
    public class EntropyBuff : BuffDebuff
    {
        public int EntropyBuffMaxStacks = 4;
        public float EntropyBuffChargeCooldown = 5;
        public float EntropySpeedBuffAmount = 60;
        public float EntropySpeedBuffDuration = .75f;
        public float EntropySlowAmount = 60;
        public float EntropySlowDuration = .75f;

        public EntropyBuff()
        {
            Name = "Entropy stacks";
            MaximumStacks = EntropyBuffMaxStacks;
        }
    }
}