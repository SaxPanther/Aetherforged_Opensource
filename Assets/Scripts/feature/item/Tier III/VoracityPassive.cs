﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Voracity Passive", menuName = "Passives/Items/Voracity")]
    public class VoracityPassive : Passive
    {
        public float VoracityBuffLifeDrainPerStack = 3;
        public int VoracityBuffMaxStacks = 5;
        public float VoracityBuffDuration = 4;
        public int VoracityPassiveStacksForger = 2;

        public static BuffDebuff VoracityBuff;

        public VoracityPassive()
        {
            Name = "Voracity passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (packet.Kind == DamageActionType.AutoAttack)
                {
                    if (target.Kind == UnitKind.Forger || target.Kind == UnitKind.LargeMonster)
                    {
                        owner.AddBuff(VoracityBuff.WithSource(pair.Source), VoracityPassiveStacksForger);
                    }
                    else
                    {
                        owner.AddBuff(VoracityBuff.WithSource(pair.Source));
                    }
                }
                return packet;
            };
        }
    }
}