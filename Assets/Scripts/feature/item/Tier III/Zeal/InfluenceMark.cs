﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Influence Mark", menuName = "Passives/Items/Influence Mark")]
    public class InfluenceMark : BuffDebuff
    {
        public int InfluenceBuffBuildingMaxStacks = 3;
        public float InfluenceSpeedBuffIncrease = 30;
        public float InfluenceSpeedBuffDuration = 2;
        public float InfluencePassiveBonusAttackDamage = .5f;
        public float InfluenceMarkDuration = 6;
        [SerializeField] Passive InfluencePassive;
        [SerializeField] BuffDebuff InfluenceSpeedBuff;
        [SerializeField] BuffDebuff InfluenceBuffBuilding;
        [SerializeField] BuffDebuff InfluenceBuff;


        public InfluenceMark()
        {
            Name = "Influence mark";
            AlsoObsolete_Duration = (pair) =>
            {
                return InfluenceMarkDuration;
            };
            BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) =>
            {
                if (packet.Source != pair.Source)
                {
                    packet.Source.AddBuff(InfluenceBuff.WithSource(pair.Source));
                    packet.Source.AddBuff(InfluenceBuffBuilding.WithSource(pair.Source));
                    if (packet.Source.StacksOf(InfluenceBuffBuilding.WithSource(pair.Source)) >= InfluenceBuffBuilding.MaximumStacks)
                    {
                        packet.Source.RemoveBuff(InfluenceBuffBuilding.WithSource(pair.Source), fullClear: true);
                        //TODO: Make this an actual speed buff once they're a separate thing
                        packet.Source.AddBuff(InfluenceSpeedBuff.WithSource(pair.Source));
                        //TODO: Make the source of the attack do a second basic attack instantly, similar to Nazarah passive
                    }
                }
                return packet;
            };
            OnRemove = (SourcePair pair, Unit owner) =>
            {
                pair.Source.GetPassiveData(pair.PassiveParent).Mark = null;
            };
        }
    }
}