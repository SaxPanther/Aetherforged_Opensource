﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Lust Passive", menuName = "Passives/Items/Lust")]
    public class LustPassive : Passive
    {
        [SerializeField]
        float LustBuffDuration = 3;
        [SerializeField]
        float LustPassiveDamagePercentage = -15f;
        [SerializeField]
        float LustBuffHealIncrease = 25f;
        [SerializeField]
        BuffDebuff LustBuff;

        public LustPassive()
        {
            Name = "Lust passive";
            BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) =>
            {
                float physicalDamagetoTakeOverTime = packet.PhysicalDamage * LustPassiveDamagePercentage;
                float magicalDamagetoTakeOverTime = packet.MagicalDamage * LustPassiveDamagePercentage;
                float totalDamageOverTime = physicalDamagetoTakeOverTime + magicalDamagetoTakeOverTime;
                DamagePacket bonusPacket = new DamagePacket(DamageActionType.ItemEffect, packet.Source);
                packet.PercentPreMagicalModifier += LustPassiveDamagePercentage;
                packet.PercentPrePhysicalModifier += LustPassiveDamagePercentage;
                bonusPacket.PureDamage += totalDamageOverTime;
                owner.ApplyDamageOverTime(bonusPacket, LustBuffDuration);
                owner.AddBuff(LustBuff.WithSource(pair.Source));
                return packet;
            };
        }
    }
}