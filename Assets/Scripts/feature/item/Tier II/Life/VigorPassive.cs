﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Vigor Passive", menuName = "Passives/Items/Vigor")]
	public class VigorPassive : Passive
	{
		[SerializeField]
		BuffDebuff VigorBuff;

		public VigorPassive()
		{
			Name = "Vigor Passive";

			AfterRemoveCC = (SourcePair pair, Unit owner, PassiveTracker data, Status removedCC) =>
			{
				owner.AddBuff(VigorBuff.WithSource(pair.Source));
			};
		}
	}
}