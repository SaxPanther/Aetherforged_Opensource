﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Vitality Passive", menuName = "Passives/Items/Vitality")]
	public class VitalityPassive : Passive
	{
		[SerializeField]
		BuffDebuff VitalityBuff;

		public VitalityPassive()
		{
			Name = "Vitality Passive";
			
			AfterTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, Unit source) =>
			{
				if(source.Kind == UnitKind.Forger || source.Kind == UnitKind.LargeMonster)
				{
					owner.AddBuff(VitalityBuff);
				}
			};
		}
	}
}