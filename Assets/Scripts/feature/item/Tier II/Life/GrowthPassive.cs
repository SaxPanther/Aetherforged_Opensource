﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;


namespace feature
{
	[CreateAssetMenu(fileName = "Growth Passive", menuName = "Passives/Items/Growth")]
	public class GrowthPassive : Passive
	{
		[SerializeField]
		BuffDebuff GrowthBuff;

		public GrowthPassive()
		{
			Name = "Growth Passive";

			OnLevelUp = (SourcePair pair, Unit owner, PassiveTracker data) =>
			{
				owner.AddBuff(GrowthBuff.WithSource(pair.Source));
			};
		}
	}
}