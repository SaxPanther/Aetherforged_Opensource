﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;


namespace feature
{
	[CreateAssetMenu(fileName = "Potential Passive", menuName = "Passives/Items/Potential")]
	public class PotentialPassive : Passive
	{
		[SerializeField]
		BuffDebuff PotentialBuff;
        [SerializeField] 
        BuffDebuff FullStackBuff;

        [SerializeField] float HealthPerStack =4f;
        [SerializeField] float PowerPerStack =.5f;
		
		public PotentialPassive()
		{
			Name = "Potential Passive";

			OnKillUnit = (SourcePair pair, Unit owner, PassiveTracker data, Unit target) =>
			{
				if(target.Kind == UnitKind.Minion || target.Kind == UnitKind.Miner 
				|| target.Kind == UnitKind.LargeMonster || target.Kind == UnitKind.Monster)
				{
                    if (owner.HasBuff(FullStackBuff.WithSource(pair.Source)))
                    {
                        // We're already full!~~
                    }
                    else if (owner.StacksOf(PotentialBuff.WithSource(pair.Source)) >= PotentialBuff.MaximumStacks)
                    {
                        owner.AddBuff(FullStackBuff.WithSource(pair.Source));
                        owner.RemoveBuff(PotentialBuff.WithSource(pair.Source), fullClear: true);
                    }
                    else
                    {
                        owner.AddBuff(PotentialBuff.WithSource(pair.Source));

                    }

                    owner.RecalculateDynamicStats(pair);

				}

			};

            DynamicStats = (pair, owner, data) =>
            {
                if (owner.HasBuff(FullStackBuff.WithSource(pair.Source)))
                {
                    float fullStackHealth = HealthPerStack * PotentialBuff.MaximumStacks;
                    float fullStackPower = PowerPerStack * PotentialBuff.MaximumStacks;
                    return UnitStats.Stats(MaxHealth: fullStackHealth, Power: fullStackPower);
                }

                // Otherwise health and power for each stack
                float totalHealth = HealthPerStack * owner.StacksOf(PotentialBuff, pair.Source);
                float totalPower = PowerPerStack * owner.StacksOf(PotentialBuff, pair.Source);
                return UnitStats.Stats(MaxHealth: totalHealth, Power: totalPower);


            };

            // This passive has no max stacks 
            NoMaxStacks = true;
		}
	}
}