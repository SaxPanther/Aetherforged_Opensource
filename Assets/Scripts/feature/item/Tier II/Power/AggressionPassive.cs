﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Aggression Passive", menuName = "Passives/Items/Aggression")]
	public class AggressionPassive : Passive
	{
		[SerializeField]
		float AggressionPassiveBaseDamage;
		[SerializeField]
		float AggressionPassivePowerScaling;
		
		public AggressionPassive()
		{
			Name = "Aggression Passive";
			
			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
			{
				if (packet.Kind == DamageActionType.AutoAttack || packet.Kind == DamageActionType.SkillOrAbility)
				{
					float baseBonusDamage = AggressionPassiveBaseDamage + (owner.Power * AggressionPassivePowerScaling);
					DamagePacket aggressionPacket = new DamagePacket(DamageActionType.ItemEffect, owner);
					aggressionPacket.PhysicalDamage += baseBonusDamage;
					owner.DealDamage(target, aggressionPacket);
				}
				return packet;
			};
		}
	}
}