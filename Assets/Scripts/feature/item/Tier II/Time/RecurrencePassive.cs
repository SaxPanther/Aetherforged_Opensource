﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Recurrence Passive", menuName = "Passives/Items/Recurrence")]
    public class RecurrencePassive : Passive
    {
        public int MaxRecurrenceBuffStacks = 100;
        public float RecurrenceBuffStacksBuiltPerCast = 10;
        public float RecurrenceBuffStacksBuiltPerStep = 2;
        public float RecurrenceHasteBuffAmount = 15;
        public float RecurrenceHasteBuffDuration = 3;
        public float[] RecurrenceCooldown = new float[] { 3 };

        [SerializeField] BuffDebuff RecurrenceBuff;
        [SerializeField] BuffDebuff RecurrenceHasteBuff;

        public RecurrencePassive()
        {
            Name = "Recurrence passive";
            Cooldowns = RecurrenceCooldown;
            OnCastSkill = (SourcePair pair, Unit owner, Castable ability) =>
            {
                int stacksGained = Mathf.FloorToInt(pair.ability.StackValue(RecurrenceBuffStacksBuiltPerCast, 1, owner.StacksOf(pair) - 1));
                owner.AddBuff(RecurrenceBuff.WithSource(pair.Source), stacksGained);
            };
            OnMove = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTime) =>
            {
                int stacksGained = Mathf.FloorToInt(pair.ability.StackValue(RecurrenceBuffStacksBuiltPerStep, 1, owner.StacksOf(pair) - 1));
                owner.AddBuff(RecurrenceBuff.WithSource(pair.Source), stacksGained);
            };
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (packet.Kind == DamageActionType.SkillOrAbility && owner.StacksOf(RecurrenceBuff, pair.Source) >= RecurrenceBuff.MaximumStacks)
                {
                    owner.AddBuff(RecurrenceHasteBuff.WithSource(pair.Source));
                    owner.RemoveBuff(RecurrenceBuff.WithSource(pair.Source), fullClear: true);
                    owner.StartBareCooldown(pair);
                }
                return packet;
            };
            NoMaxStacks = true;
        }
    }
}