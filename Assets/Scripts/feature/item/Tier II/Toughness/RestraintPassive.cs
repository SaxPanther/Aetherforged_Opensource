﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;


namespace feature
{
	[CreateAssetMenu(fileName = "Restraint Passive", menuName = "Passives/Items/Restraint")]
	public class RestraintPassive : Passive
	{
		[SerializeField]
		BuffDebuff RestraintBuff;

		public RestraintPassive()
		{
			Name = "Restraint Passive";

			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
			{
                if (packet.Kind == DamageActionType.AutoAttack || packet.Kind == DamageActionType.SkillOrAbility)
				{
					target.AddBuff(RestraintBuff.WithSource(pair.Source));
				}
				return packet;
			};
		}
	}
}