﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "LifePassive", menuName = "Passives/Items/Life")]
    public class LifePassive : Passive
    {
        public float[] LifePassiveCooldown = new float[] { 20f };
        public float LifePassiveRegen = 15;
        public float LifePassiveDuration = 5;




        public LifePassive()
        {
			Name = "Life passive";
			Cooldowns = LifePassiveCooldown;
			AfterTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, Unit damageSource) => {
				float totalDamageHealed = pair.ability.StackValue(LifePassiveRegen, 1, owner.StacksOf(pair) - 1);
				HealPacket healPacket = new HealPacket(owner, HealActionType.HealBurst);
				if (pair.Source.Kind == UnitKind.Forger || pair.Source.Kind == UnitKind.LargeMonster) { 
					//TODO: Update receiveHealOverTime once it takes in a heal packet
					owner.Obsolete_ReceiveHealOverTime(totalDamageHealed, LifePassiveDuration, owner);
					owner.StartBareCooldown(pair);
				}
			};
			NoMaxStacks = true;
		}
    }
}