﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "ToughnessPassive", menuName = "Passives/Items/Toughness")]
    public class ToughnessPassive : Passive
    {
        public float ToughnessPassiveDamageBlock = 3;
        public float ToughnessPassiveDamageBlockForgers = 6;
        public float ToughnessPassiveEffectivenessAfterFirst = .5f;




        public ToughnessPassive()
        {
			Name = "Toughness passive";
			BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) => {
				float damageToReduce = (packet.Source.Kind == UnitKind.Forger || packet.Source.Kind == UnitKind.LargeMonster) ? ToughnessPassiveDamageBlockForgers : ToughnessPassiveDamageBlock;
				float finalDamagetoReduce = pair.ability.StackValue(damageToReduce, ToughnessPassiveEffectivenessAfterFirst, owner.StacksOf(pair) - 1);
				if (packet.Kind == DamageActionType.AutoAttack) {
					packet.FlatPhysicalDamageReduction += finalDamagetoReduce;
				}
				return packet;
			};
			NoMaxStacks = true;
		}
    }
}