﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class FogOfWar : MonoBehaviour {

    public LayerMask validLayers;
    public LayerMask sightLayers;
    public int resolution;
    public int radius;
    public float height;

    private GameObject fowPlane;

    private const int SIGHT_LAYER = 9;

    private RenderTexture renderTexture;

    void Start () {
    //    if(!GameObject.Find("FoWBackgroundPlane(Clone)")) //We're the lucky unit that gets to instantiate FoW
    //     {
    //        var pane = (GameObject)Instantiate(Resources.Load("FoWBackgroundPlane"));
    //        pane.transform.position = new Vector3(0, -10, 0);

    //         var fowCamera = GameObject.Find("FoW Camera").GetComponent<Camera>();

    //        fowCamera.targetTexture = renderTexture;
    //        renderTexture = new RenderTexture((int)fowCamera.pixelRect.width, (int)fowCamera.pixelRect.height, 16, RenderTextureFormat.ARGB32);
    //        renderTexture.Create();

    //         {
    //             var gob = GameObject.Find("FogOfWarUIPanel");
    //             if (gob == null) {
    //                 Debug.LogErrorFormat("{1} not found. Turning off FogOfWar script on {0}", this.name, "FogOfWarUIPanel");
    //                 this.enabled = false;

    //                 return;
    //             }
    //         }

    //        GameObject.Find("FogOfWarUIPanel").GetComponent<RawImage>().texture = renderTexture;
    //        GameObject.Find("FogOfWarUIPanel").GetComponent<RawImage>().color = new Color(0, 0, 0, 0.5f);
    //     }

        // //Generate plane to use for drawing line of sight
        // fowPlane = new GameObject("LineOfSightPlane");
        // fowPlane.transform.SetParent(transform);
        // fowPlane.layer = SIGHT_LAYER;
        // MeshFilter meshFilter = (MeshFilter)fowPlane.AddComponent(typeof(MeshFilter));
        // MeshRenderer renderer = fowPlane.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        // renderer.material.shader = Shader.Find("Custom/FoWLight"); //Custom shader that clears out the darkness
        // Texture2D tex = new Texture2D(1, 1);
        // tex.SetPixel(0, 0, Color.white);
        // tex.Apply();
        // renderer.material.mainTexture = tex;
        // renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        // renderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
    // }

    // Mesh CreateMesh(Vector3[] vertices3D)
    // {
    //     Vector2[] vertices2D = new Vector2[vertices3D.Length];
    //     for (int i = 0; i < vertices3D.Length; i++)
    //     {
    //         vertices2D[i] = new Vector2(vertices3D[i].x, vertices3D[i].z);
    //     }

    //     Triangulator tr = new Triangulator(vertices2D);
    //     int[] indices = tr.Triangulate();

    //     Vector3[] vertices = new Vector3[vertices2D.Length];
    //     for (int i = 0; i < vertices.Length; i++)
    //     {
    //         vertices[i] = new Vector3(vertices2D[i].x, height, vertices2D[i].y);
    //     }

    //     Mesh msh = new Mesh();
    //     msh.vertices = vertices;
    //     msh.triangles = indices;
    //     msh.RecalculateNormals();
    //     msh.RecalculateBounds();
    //     return msh;
    // }

    // void Update () {
    //     Vector3[] fowMeshPoints = new Vector3[resolution];
    //     var fowMeshPointIndex = 0;
    //     for(float i = 0; i < Mathf.PI * 2; i += Mathf.PI * 2 / resolution) //Circles!
    //     {
    //         if(fowMeshPointIndex >= resolution) break; //Precision error, please ignore.
    //         RaycastHit hitInfo; //Probably useful later
    //         if(Physics.Raycast(transform.position + Vector3.up * height, new Vector3(Mathf.Cos(i), 0, Mathf.Sin(i)), out hitInfo, radius, validLayers))
    //         {
    //             fowMeshPoints[fowMeshPointIndex] = hitInfo.point - transform.position;
    //         } else
    //         {
    //             fowMeshPoints[fowMeshPointIndex] = Vector3.up * height + new Vector3(Mathf.Cos(i) * radius, 0, Mathf.Sin(i) * radius);
    //         }
    //         fowMeshPointIndex++;
    //     }
    //     fowPlane.GetComponent<MeshFilter>().mesh = CreateMesh(fowMeshPoints);
    // }
    // void LateUpdate() {
    //     if (fowPlane != null) {
    //         fowPlane.transform.rotation = Quaternion.Euler(0, 0, 0);
    //         fowPlane.transform.position = transform.position;
    //     }
    }
}
