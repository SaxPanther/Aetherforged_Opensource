﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "IntensiveStudy", menuName = "Ability/Visionary/IntensiveStudy")]
    public class IntensiveStudy : Castable
    {
        #region intensive study variables
        //intensive study base variables
        public float[] intensiveStudyCooldown = new float[] { 1, 1, 1, 1, 1 };
        //intensive study debuff variables
        public BuffDebuff intensiveStudyDebuff;
        public float intensiveStudyDebuffDuration = 5;
        public bool intensiveStudyReducePower = false;
        public bool intensiveStudyReapply = false;
        public float[] intensiveStudyPowerReduction = new float[] { 1, 1, 1, 1, 1 };
        public float[] intensiveStudyResistanceReduction = new float[] { 1, 1, 1, 1, 1 };
        public float intensiveStudyDebuffAmp = 2.0f;
        public float intensiveStudyReappDuration = 2.5f;
        #endregion

        public IntensiveStudy ()
        {
            Name = "intensive_study";
            //Cooldowns = intensiveStudyCooldown;
            TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple;
            UnitTargetedCast = (Castable ability, Unit caster, Unit target) =>
            {
                intensiveStudyReapply = false;
                intensiveStudyReducePower = false;
                caster.PlayAnimation("Ability2", target.Position);
                    /*need logic for determining how much hephisalt a character has spent on a particular stat
                    float saltSpentOnOffense = target.saltSpentOnPower + target.saltSpentOnHaste + target.saltSpentOnLifeDrain;
                    float saltSpentOnDefense = target.saltSpentOnHealth + target.saltSpentOnArmor + target.saltSpentOnResistance;
                    if(saltSpentOnOffense > saltSpentOnDefense) {
                        intensiveStudyReducePower = true;
                    }
                    */
                caster.StartCooldown(ability);
                target.AddBuff(intensiveStudyDebuff);
            };
        }
    }
}