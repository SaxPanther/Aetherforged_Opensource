﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Cold Steel Buff", menuName = "Ability/Ember/Cold Steel Buff")]
	public class ColdSteelBuff : BuffDebuff
	{
		[SerializeField] float[] damage;
		[SerializeField] float powerScaling;
		[SerializeField] float applications;
		[SerializeField] float innerFireCost;
		[SerializeField] float innerFireGain;
		[SerializeField] float healthThreshold;
		[SerializeField] float cooldownReduction;
		[SerializeField] BuffDebuff ColdSteelBuffRef;
		[SerializeField] BuffDebuff ColdSteelShield;
		private float count = 0;
		private float cooldownNew;
		private float currentCooldown;
		private bool addShield;
		public ColdSteelBuff()
		{
			OnApply = (pair, holder) =>
			{
				count = 0;
				cooldownNew = pair.CastableParent.Cooldowns[pair.Source.GetAbilityLevelLessOne(pair.CastableParent)];
			};

			OnBasicAttack = (pair, holder, target, packet) =>
			{
				count += 1;
				float totalDamage = damage[holder.GetAbilityLevelLessOne(pair.ability)] 
					+ powerScaling * holder.Power;

				if(count <= applications)
				{
					packet.PhysicalDamage += totalDamage;
					holder.PlayAnimation("Ability1");
					//holder.AddBuff(ColdSteelShield.WithSource(pair.Source)); //Adding a buff on a basic attack gives an error
				}

				if(holder.Resource.Amount >= innerFireCost && count <= applications)
				{
					float cooldownMax = pair.CastableParent.Cooldowns[pair.Source.GetAbilityLevelLessOne(pair.CastableParent)];
					float reduction = cooldownMax * cooldownReduction;
					cooldownNew -= reduction;

					holder.Resource -= innerFireCost;
					
					//Check what are gain was and reduce it
					if(target.HPRatio <= healthThreshold)
					{
						holder.Resource -= innerFireGain * 2;
					}
					else
					{
						holder.Resource -= innerFireGain;
					}
				}
				return packet;
			};

			OnRemove = (pair, holder) =>
			{
				holder.StartCooldown(pair.CastableParent);
				holder.SetCooldown(pair.CastableParent, cooldownNew, true);
			};
		}
	}
}