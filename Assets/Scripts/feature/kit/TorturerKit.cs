﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;
using core.utility;
using managers;

namespace feature.kit
{
    public class TorturerKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new AbilityBruiser();
            }
        }
        */
        #region cruelty variables
        //cruelty base variables
        public float[] crueltyCooldown = new float[] { 5 };
        //cruelty buff variables
        public static BuffDebuff CrueltyBuff;
        public float crueltyDuration = 4f;
        public float crueltyMaxStacks = 5;
        public float crueltyCurrentStacks = 0;
        //sadism buff variables
        public static BuffDebuff CrueltySadismBuff;
        public float crueltySadismDuration = 3f;
        public float crueltySadismPowerPerLevel = 3;
        public float crueltySadismCurrentPower = 0;
        #endregion

        #region gut 'em variables
        //gut 'em base variables
        public float[] gutEmCooldown = new float[] { 12, 11.5f, 11, 10.5f, 10 };
        public int gutEmRange = 330;
        public int gutEmWidth = 200;
        //gut 'em damage variables
        public float[] gutEmBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float gutEmBasePowerScaling = .1f;
        //gut 'em dash variables
        public float gutEmDashRange = 150;
        public float gutEmDashSpeed = 500;
        //gut 'em recast variables
        public float gutEmGlobalCooldownDuration = .25f;
        public Timer gutEmReactivationTimer = null;
        public float gutEmReactivationTimerDuration = 2;
        public float gutEmCurrentRecasts = 0;
        public float gutEmMaxRecasts = 2;
        //gut 'em debuff variables
        public BuffDebuff gutEmOpenVeinDebuff;
        public float openVeinStackDuration = 10;
        public float openVeinMaxStacks = 3;
        //gut 'em debuff damage variables
        public float[] openVeinBleedDamage = new float[] { 1, 1, 1, 1, 1 };
        public float openVeinBleedPowerScaling = .1f;
        public float openVeinBleedDuration = 1.5f;
        //gut 'em debuff CC variables
        public float openVeinSlowAmount = .4f;
        public float openVeinSlowDuration = 1.25f;
        public float openVeinStunDuration = .5f;
        #endregion

        #region interrogation tactics variables
        //interrogation tactics base variables
        public float[] interrogationTacticsCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float interrogationTacticsAoERadius = 225;
        //interrogation tactics recast variables
        public float interrogationTacticsGlobalCooldownDuration = .25f;
        public Timer interrogationTacticsReactivationTimer;
        public float interrogationTacticsReactivationDuration = 3.5f;
        public float interrogationTacticsCurrentRecasts = 0;
        public float interrogationTacticsMaxRecasts = 1;
        //interrogation tactics buff variables
        public BuffDebuff interrogationTacticsBuff;
        public float interrogationTacticsBuffDuration = 3.5f;
        //interrogation tactics buff damage variables
        public float[] interrogationTacticsBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float interrogationTacticsPowerScaling = .5f;
        //interrogation tactics buff heal variables
        public float[] interrogationTacticsMaxHealPercentage = new float[] { .4f, .425f, .45f, .475f, .5f };
        public float[] interrogationTacticsBaseHealPercentage = new float[] { .2f, .25f, .3f, .35f, .4f };
        public float interrogationTacticsHealPercentagePerEnemy = .04f;
        public float interrogationTacticsCurrentDamageTaken = 0;
        public float interrogationTacticsEnemiesHit = 0;
        #endregion

        #region wicked slice variables
        //wicked slice base variables
        public float[] wickedSliceCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float wickedSliceRange = 220;
        //wicked slice damage variables
        public float[] wickedSliceBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float wickedSlicePowerScaling = .5f;
        //wicked slice CC variables
        public float wickedSliceSlowAmount = .35f;
        public float wickedSliceSlowDuration = 1.5f;
        public float wickedSliceFlipDistance = 125;
        public float wickedSliceFlipDuration = .5f;
        #endregion

        #region vindictive flagellation variables
        //vindictive flagellation base variables
        public float[] vindictiveFlagellationCooldown = new float[] { 1, 1, 1 };
        public float vindictiveFlagellationRange = 575;
        //vindictive flagellation projectile variables
        public float vindictiveFlagellationMissileSpeed = 1000;
        public float vindictiveFlagellationWidth = 75;
        public string vindictiveFlagellationProjectile = "vindictive_flagellation_projectile";
        //vindictive flagellation damage variables
        public float[] vindictiveFlagellationBaseDamage = new float[] { 1, 1, 1 };
        public float vindictiveFlagellationPowerScaling = .5f;
        //vindictive flagellation debuff variables
        public BuffDebuff vindictiveFlagellationDebuff;
        public float vindictiveFlagellationDebuffDuration = 4.5f;
        public float vindictiveFlagellationMaxRange = 650;
        //vindictive flagellation debuff damage variables
        public float[] vindictiveFlagellationBleedDamage = new float[] { 1, 1, 1 };
        public float vindictiveFlagellationBleedPowerScaling = .1f;
        //vindictive flagellation debuff CC variables
        public float vindictiveFlagellationCurrentPulls = 0;
        public float vindictiveFlagellationMaxPulls = 2;
        public float vindictiveFlagellationBasePullDistance = 100;
        public float vindictiveFlagellationPullRangeIncrease = 50;
        public float vindictiveFlagellationPullDuration = .25f;
        #endregion
        /*
        public override string ObsoleteName { get { return "eymric"; } }
        */
        protected override void Initialize(Unit me)
        {
            #region passive ability
            /*
            CrueltyBuff = new BuffDebuff()
            {
                Name = "cruelty_buff",
                MaximumStacks = 5,
                Obsolete_OnApply = (Passive self, Unit source, Unit target) =>{
                    if(self.CurrentStacks >= self.MaximumStacks){
                        //set the base effect on cooldown and apply the new buff
                        self.Parent.StartCooldown(source);
                        CrueltySadismBuff.Parent = self;
                        target.AddBuff(CrueltySadismBuff);
                        target.RemoveBuff(CrueltyBuff);
                    }
                },
                Obsolete_Duration = (Passive self) => {
                    return crueltyDuration;
                }
            };
            */
            CrueltySadismBuff = new BuffDebuff() {
                Name = "sadism_buff",

                /*
                AdjustStats = (Passive self) =>{
                    Unit source = self.Source;
                    crueltySadismCurrentPower = crueltySadismPowerPerLevel * source.Level;
                    return UnitStats.Stats(Power:crueltySadismCurrentPower);
                },
                Obsolete_Duration = (Passive self) => {
                    return crueltySadismDuration;
                }
                */
            };

            float[] SadismPenetration = new float[] { 5, 10, 15, 20 };

            var crueltyPassive = new Passive()
            {
                Name = "cruelty",
                Cooldowns = crueltyCooldown,
                BeforeDealDamage = (pair, holder, target, packet) =>
                {
                    // FIXME check if this is the right place to look
                    if (target.Disables.MovementDisablingStatuses.Count > 0)
                    {
                        packet.MagicalPenetration = SadismPenetration[pair.AbilityLevelLessOne];
                        packet.PhysicalPenetration = SadismPenetration[pair.AbilityLevelLessOne];
                        holder.StartCooldown(pair);
                    }
                    return packet;
                }
                /*
                Obsolete_OnDealDamage = (Passive ability, Unit caster, Unit target, DamagePacket packet) => {
                    
                    //remove the buff and reset the timer
                    caster.AddBuff(CrueltyBuff, parent: ability, source: caster);
                    return packet;
                }
                */
            };
            Passives.Add(0, crueltyPassive);
            #endregion

            #region first active
            gutEmOpenVeinDebuff = new BuffDebuff()
            {
                Name = "open_vein_debuff",
                MaximumStacks = 3,
                //TODO
                /*
                Obsolete_OnRemove = (Passive self, Unit debuffedUnit, Unit source) => {
                      //needs to know who created the buff
                      //Also needs to know what the parent of the buff is and number of current stacks
                     float totalDamage = openVeinBleedDamage[self.Parent.Level-1] + (openVeinBleedPowerScaling * source.Power);
                    /*
                    if (self.CurrentStacks == 1){
                          //apply bleed
                          //debuffedUnits.TakeDamageOverTime(new DamagePacket{ physicalDamage = totalDamage}, openVeinBleedDuration);
                     }
                     if(self.CurrentStacks == 2){
                          //apply slow
                          //debuffedUnit.AddBuff(new Slow(openVeinSlowAmount, openVeinSlowDuration));
                     }
                     if(self.CurrentStacks >= 3){
                          //apply stun
                          //debuffedUnit.AddBuff(new Stun(gutEmOpenVeinStunDuration));
                     }
                    
                 },
                    */
                Obsolete_Duration = (Passive self) => {
                    return openVeinStackDuration;
                }
            };

            var gutEmActive = new Castable()
            {
                Name = "gut_em",
                Cooldowns = gutEmCooldown, //need to update this with a scaling cooldown
                GetIconSubtext = (ability, caster) =>
                {
                    return 10.ToString();
                },
                OnNonTargetedCast = (Castable ability, Unit caster) =>
                {
                    float totalBaseDamage = gutEmBaseDamage[ability.Level - 1] + (gutEmBasePowerScaling * caster.Power);
                    caster.PlayAnimation("Ability1");

                    Vector2 locationTarget;
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, gutEmDashRange))
                    {
                        caster.Dash(ability, locationTarget, gutEmDashSpeed,
                            onStop: (dash) => {
                                ForEnemiesInRadius(gutEmWidth, caster, (Unit enemy) =>
                                {
                                    caster.DealDamage(enemy, new DamagePacket { PhysicalDamage = totalBaseDamage });
                                    enemy.AddBuff(gutEmOpenVeinDebuff);
                                });
                                if (gutEmCurrentRecasts >= gutEmMaxRecasts)
                                {
                                    //TODO
                                    //put on full cooldown
                                    //also, pop all stacks of open vein on all enemies
                                    gutEmCurrentRecasts = 0;
                                }
                                else
                                {
                                    //TODO
                                    //put on shorter cooldown and start the recast window, which sets on full cooldown and pops all stacks of open vein
                                }
                            });
                    }
                },
            };
            Skills.Add(1, gutEmActive);
            #endregion

            #region second active
            interrogationTacticsBuff = new BuffDebuff()
            {
                Name = "interrogation_tactics_buff",
                //TODO
                Obsolete_OnTakeDamagePostMit = (Passive ability, Unit source, Unit buffedUnit, DamagePacket damagePacket) => {

                    interrogationTacticsCurrentDamageTaken += 100 /*damagePacket.convertAllFinalDamage()*/; //or however we determine exactly what the final amount of damage is, after applying all defensive stats, penetration, damage increase, and dr
                    return damagePacket;
                },
                OnRemove = (SourcePair pair, Unit buffedUnit) => {
                    float finalPercentageHealed = interrogationTacticsBaseHealPercentage[pair.ParentAbilityLevelLessOne] + (interrogationTacticsHealPercentagePerEnemy * interrogationTacticsEnemiesHit);
                    float finalRawHeal = finalPercentageHealed * interrogationTacticsCurrentDamageTaken;
                    float currentMaxHeal = buffedUnit.MaxHealth * interrogationTacticsMaxHealPercentage[pair.ParentAbilityLevelLessOne];
                    float finalHealAmount = (finalRawHeal >= currentMaxHeal) ? finalRawHeal : currentMaxHeal;
                    HealPacket packet = new HealPacket(HealActionType.HealBurst, pair.Source) { HealAmount = finalHealAmount };
                    pair.Source.ProvideHeal(buffedUnit, packet);
                    //add damage here TODO
                },
                Obsolete_Duration = (Passive self) => {
                    return interrogationTacticsBuffDuration;
                }
            };

            var interrogationTacticsActive = new Castable()
            {
                Name = "interrogation_tactics",
                Cooldowns = interrogationTacticsCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    if (interrogationTacticsCurrentRecasts <= 0)
                    {
                        interrogationTacticsCurrentDamageTaken = 0;
                        interrogationTacticsEnemiesHit = 0;
                        interrogationTacticsCurrentRecasts++;
                        interrogationTacticsBuff.Parent = ability;
                        caster.AddBuff(interrogationTacticsBuff);
                        //add a reactivation that allows it to be removed that ends the window when the buff ends
                    }
                    else {
                        interrogationTacticsCurrentRecasts = 0;
                        caster.RemoveBuff(interrogationTacticsBuff);
                        //set on full cooldown
                    }
                }
            };
            Skills.Add(2, interrogationTacticsActive);
            #endregion

            #region third active
            var wickedSliceActive = new Castable()
            {
                Name = "wicked_slice",
                Cooldowns = wickedSliceCooldown,
                TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
                UnitTargetedCast = (Castable ability, Unit caster, Unit target) => {
                    float totalDamage = wickedSliceBaseDamage[ability.Level - 1] + (wickedSlicePowerScaling * caster.Power);
                    //TODO
                    //will need to implement a boolean that triggers when a unit is effected by hard CC and use that here instead
                    /*if (target.IsHardCCed) {
                     * //flip unit over torturer 125 units behind him
                    }
                    else{*/
                    //target.AddBuff(new Slow(wickedSliceSlowAmount, wickedSliceSlowDuration));
                        
                }
            };
            Skills.Add(3, wickedSliceActive);
            #endregion

            #region fourth active
            var vindictiveFlagellationActive = new Castable()
            {
                Name = "vindictive_flagellation",
                Cooldowns = vindictiveFlagellationCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    vindictiveFlagellationCurrentPulls = 0;
                    //throw a chain that stops on the first enemy hit and deals damage to them
                    //applies bleed that gets removed when the first debuff gets removed
                    Vector2 locationTarget;
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, gutEmDashRange)) {
                    }
                }
            };
            Skills.Add(4, vindictiveFlagellationActive);
            #endregion
        }
    }
}