﻿using core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace feature.kit {
    public class GroundEffect {
        public object Position;
        public Action<Unit> OnUnitLeave;
        public Action<Unit> OnUnitEnter;
        public int Radius;
        public float Duration;

        List<Unit> unitsInRadius;

        public GroundEffect() {
            var isDone = false;
            new Timer().Start(() => {
                isDone = true;
                return 0f;
            }, Duration);
            new Timer().Start(() => {
                if(isDone) {
                    return 0f;
                }
                Vector2 position = new Vector2(0, 0);
                if(Position.GetType().IsAssignableFrom(typeof(Unit))) {
                    position = ((Unit)Position).Position;
                }
                List<Unit> unitsRemoved = new List<Unit>(unitsInRadius);
                Unit.ForEachInRadius(Radius, position, (unit) => {
                    
                    if(!unitsInRadius.Contains(unit)) {
                        unitsInRadius.Add(unit);
                        if (OnUnitEnter != null)
                            OnUnitEnter(unit);
                    }
                    unitsRemoved.Remove(unit);
                });

                if(OnUnitLeave != null) {
                    foreach (Unit unit in unitsRemoved) {
                        OnUnitLeave(unit);
                    }
                }
                

                return 0.1f;
            });
        }

        //TODO make it possible to check for only allies or enemies
        [Obsolete("Please use Unit.InRadius() or Unit.ForEachInRadius(), Unit.ForEachAllyInRadius() and Unit.ForEachEnemyInRadius(), or ForEnemiesInRadius() and ForAlliesInRadius() under kit.")]
        public void ForUnitsInRadius(int radius, Vector2 position, Action<Unit> action)
        {
            for (int i = 0; i < Unit.ListOf.Count; i++)
            {
                var unit = Unit.ListOf[i];
                var distance = (position -
                    new Vector2(unit.transform.position.x, unit.transform.position.z)).magnitude;
                if (distance * Constants.TO_AETHERFORGED_UNITS < radius)
                {
                    action(unit);
                }
            }
        }
    }

}
