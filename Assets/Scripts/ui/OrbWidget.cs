﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using feature.map;
using managers;

public class OrbWidget : MonoBehaviour {
    public List<OrbAssociation> links;
        

	// Use this for initialization
	void Start () {
        MineCapture[] wc = FindObjectsOfType<MineCapture>();

        links.Clear();
        // FIXME this is so bad. redo at some point
        foreach (MineCapture mine in wc) {
            var orbThing = transform.Find(string.Format("{0} Orb", mine.spot)).GetComponent<Image>();

            links.Add(new OrbAssociation() {
                spot = mine.spot, 
                mine = mine,
                orb = orbThing
            });
        }
	}


	// Update is called once per frame
	void Update () {

        foreach (var orbal in links) {
            if (orbal.mine != null) {
     
                orbal.UpdateColor ();

            }
        }
		
	}
}

[System.Serializable]
public class OrbAssociation {
    public WellSpot spot;
    public Image orb;

    public void SetMine( MineCapture mine ) {
        // TODO 

    }

    public void UpdateColor( ) {
        orb.color = UnitManager.Instance.AppropriateColour(mine.Current);

    }

    public MineCapture mine;
}