﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ui;
using controllers;

public class AbilityIndicatorHandler : MonoBehaviour {

	public GameObject indicator;
	public Vector3 OriginPos;
	public Vector3 targetpos;
	public float minrange;
	public float maxrange;
	public float width;
	public bool targetmouse;
	public bool aoeindicator;
	public bool followcaster;
	public Transform castertransform;
	public bool followtarget;
	public Transform targetransform;
	private RaycastHit hit;

	// Use this for initialization
	void Start () {
		indicator = gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	if (targetmouse == true) {
		Physics.Raycast (CameraController.Instance.camera.ScreenPointToRay (Input.mousePosition), out hit, 200, 1 << LayerMask.NameToLayer ("Ground"));
	}	
	if(maxrange==default(float)){
		maxrange = minrange;	
	}

	if(aoeindicator==true){
			

		indicator.transform.localScale = new Vector3 (maxrange,1,maxrange);
		if (targetmouse == true) {
			indicator.transform.position = hit.point;
		}	

	}
	else{
			
		indicator.transform.localScale = new Vector3 (width,1,maxrange);

		if (targetmouse) {
			targetpos = hit.point;
		}
		if(followcaster){
			OriginPos = castertransform.position;
		}
		if(followtarget & !targetmouse){
			targetpos = targetransform.position;
		}
		
		ui.AbilityIndicator.HandleLineIndicator(indicator,OriginPos,targetpos,minrange,maxrange,width);

	}
}
}