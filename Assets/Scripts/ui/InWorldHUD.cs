﻿using UnityEngine;

namespace ui
{
  public class InWorldHUD : MonoBehaviour
  {

    public Camera playerCamera;
    
    void Start ()
    {
      playerCamera = Camera.main;
    }
    void LateUpdate ()
    {
      transform.LookAt( transform.position + playerCamera.transform.rotation * Vector3.forward,
        playerCamera.transform.rotation * Vector3.up );
    }
  }
}