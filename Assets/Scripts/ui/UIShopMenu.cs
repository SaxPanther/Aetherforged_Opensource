﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using System.Reflection;

using managers;
using config;
using feature;
using core;
using core.forger;
using core.utility;
using controllers;

namespace ui {
    class UIShopMenu : MonoBehaviour {

        public GameObject TreePanel;
		
        public GameObject ListPanel;

        public GameObject ItemPanel;

        public GameObject CollumnPanel;

        public GameObject ItemBox;

        private GameObject T1CollumnPanel;

        private GameObject T2CollumnPanel;

        private GameObject T3CollumnPanel;

        private GameObject ConsumableSlot;

        private GameObject InventorySlot1;

        private GameObject InventorySlot2;

        private GameObject InventorySlot3;

        private GameObject InventorySlot4;

        private GameObject InventorySlot5;

        private GameObject InventorySlot6;

        private GameObject[] InventorySlots;

        private GameObject SelectionImage;

        private GameObject SelectionNameText;

        private GameObject SelectionStatsText;

        private GameObject SelectionCostText;

        private GameObject SelectionPassiveText;

        private GameObject TopSaltText;

        private GameObject BottomSaltText;

        private GameObject BuyButton;
        
        private GameObject SellButton;

        private GameObject BottomItemSlot1;

        private GameObject BottomItemSlot2;

        private GameObject BottomItemSlot3;

        private GameObject BottomItemSlot4;

        private GameObject BottomItemSlot5;

        private GameObject BottomItemSlot6;

        private GameObject[] BottomItemSlots;

        private GameObject InvSelection = null;

        private ItemLoader itemLoader;

        private Dictionary<string, Item> items = new Dictionary<string, Item>();

        private Item currentItem = null;

        private PlayerKnob Player = null;

        private bool isInvSelected = false;

        Dictionary<GameObject, Item> ObjectAssociation = new Dictionary<GameObject, Item>();

        public void Awake()
        {
            T1CollumnPanel = Instantiate(CollumnPanel, TreePanel.transform);
            T1CollumnPanel.SetActive(false);
            T2CollumnPanel = Instantiate(CollumnPanel, TreePanel.transform);
            T2CollumnPanel.SetActive(false);
            T3CollumnPanel = Instantiate(CollumnPanel, TreePanel.transform);
            T3CollumnPanel.SetActive(false);
            
            var forgerInventory = this.transform.Find("ForgerInventory");
            
            ConsumableSlot = Instantiate(ItemBox, this.transform);
            ConsumableSlot.transform.localPosition = new Vector3(28, -178, 0);
            GameObject InvSlotButton = ConsumableSlot.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            ConsumableSlot.SetActive(false);
            InventorySlot1 = Instantiate(ItemBox, forgerInventory);
            InvSlotButton = InventorySlot1.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            InventorySlot1.SetActive(false);
            InventorySlot2 = Instantiate(ItemBox, forgerInventory);
            InvSlotButton = InventorySlot2.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            InventorySlot2.SetActive(false);
            InventorySlot3 = Instantiate(ItemBox, forgerInventory);
            InvSlotButton = InventorySlot3.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            InventorySlot3.SetActive(false);
            InventorySlot4 = Instantiate(ItemBox, forgerInventory);
            InvSlotButton = InventorySlot4.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            InventorySlot4.SetActive(false);
            InventorySlot5 = Instantiate(ItemBox, forgerInventory);
            InvSlotButton = InventorySlot5.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            InventorySlot5.SetActive(false);
            InventorySlot6 = Instantiate(ItemBox, forgerInventory);
            InvSlotButton = InventorySlot6.transform.Find("InventorySlotButton").gameObject;
            InvSlotButton.GetComponent<Button>().onClick.AddListener(OnInventoryClick);
            InventorySlot6.SetActive(false);
            InventorySlots = new GameObject[] {InventorySlot1, InventorySlot2, InventorySlot3, InventorySlot4, InventorySlot5, InventorySlot6, ConsumableSlot};
            BuyButton = this.transform.Find("BuyButton").gameObject;
            SellButton = this.transform.Find("SellButton").gameObject;
            SelectionImage = this.transform.Find("SelectedItemImage").gameObject;
            SelectionNameText = this.transform.Find("SelectedItemName").gameObject;
            SelectionCostText = this.transform.Find("SelectedItemCost").gameObject;
            SelectionStatsText = this.transform.Find("SelectedItemStats").gameObject;
            SelectionPassiveText = this.transform.Find("SelectedItemPassive").gameObject;
            TopSaltText = this.transform.Find("TopSaltText").gameObject;
            BottomSaltText = this.transform.Find("BottomSaltText").gameObject;
            Transform BottomPanel = transform.parent.Find("BottomPanel");
            Transform ItemPanel = BottomPanel.Find("ItemsPanel");
            Transform ItemsPanel = ItemPanel.Find("Items");
            BottomItemSlot1 = ItemsPanel.Find("InventorySlots/InventorySlot1").gameObject;
            BottomItemSlot2 = ItemsPanel.Find("InventorySlots/InventorySlot2").gameObject;
            BottomItemSlot3 = ItemsPanel.Find("InventorySlots/InventorySlot3").gameObject;
            BottomItemSlot4 = ItemsPanel.Find("InventorySlots/InventorySlot4").gameObject;
            BottomItemSlot5 = ItemsPanel.Find("InventorySlots/InventorySlot5").gameObject;
            BottomItemSlot6 = ItemsPanel.Find("InventorySlots/InventorySlot6").gameObject;
            BottomItemSlots = new GameObject[] {BottomItemSlot1, BottomItemSlot2, BottomItemSlot3, BottomItemSlot4, BottomItemSlot5, BottomItemSlot6};
            Player = UnitManager.Instance.LocalKnob;
            itemLoader = GetComponent<ItemLoader>();
            items = itemLoader.InitItems();
        }

        public void Start() {
            SetupTreePanel();
            Player = UnitManager.Instance.LocalKnob;
        }

        public void OnEnable() {

            Player = UnitManager.Instance.LocalKnob;

            this.OpenTreePanel();
            int i = 0;
            if(Player != null && Player.ControlForger != null && Player.ControlForger.items.Count > 0)
            {
                foreach(Item item in Player.ControlForger.items)
                {
                    if(item != null)
                    { 
                        GameObject InventoryIcon = InventorySlots[i].transform.Find("InventorySlotButton").gameObject;
                        
                        if(Resources.Load<Sprite>("Items/ItemIcons/" + item.Name) != null)
                        {
                            InventoryIcon.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Items/ItemIcons/" + item.Name);
                        }
                        else
                        {
                            InventoryIcon.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Portraits/Ainsley128");
                        }
                        GameObject InventoryName = InventorySlots[i].transform.Find("ItemName").gameObject;
                        InventoryName.GetComponent<Text>().text = item.Name;
                        i++;
                    }
                }
            }
            foreach(GameObject slot in InventorySlots)
            {
                slot.SetActive(true);
            }

            if (Player != null && Player.ControlForger != null)
            {
                Player.ControlForger.itemNames.Callback = (UnityEngine.Networking.SyncList<string>.Operation op, int itemIndex) => {
                    RedrawItems();
                };
            }
        }
        
        private void SetupTreePanel() {

            var itemList = itemLoader.firstTier.TopLevelItems;
            
            {
                foreach (var item in itemList){
                    GameObject PanelName = Instantiate(ItemPanel, T1CollumnPanel.transform);
                    PanelName.SetActive(true);
                    GameObject ItemName = PanelName.transform.Find("ItemName").gameObject;
                    ItemName.GetComponent<Text>().text = item.Name;
                    GameObject ItemIcon = PanelName.transform.Find("ItemIcon").gameObject;
                    ItemIcon.GetComponent<Image>().overrideSprite =
                                item.icon;
                        //Resources.Load<Sprite>("Items/ItemIcons/"+ item.name);
                    GameObject ItemCost = PanelName.transform.Find("ItemCost").gameObject;
                    ItemCost.GetComponent<Text>().text = items[item.name].Cost.ToString();
                    GameObject ItemButton = PanelName.transform.Find("ItemButton").gameObject;
                    ItemButton.GetComponent<Button>().onClick.AddListener(OnItemClick);
                    ObjectAssociation.Add(PanelName, item);
                }
                T1CollumnPanel.SetActive(true);
            }
            //Original code by VigorouslySalted
        }

        public void OnInventoryClick() 
        {
            if(itemLoader == null)
            {
                itemLoader = GetComponent<ItemLoader>();
                items = itemLoader.InitItems();
            }

            if(items.Count == 0)
            {
                items = itemLoader.InitItems();
            }

            InvSelection = EventSystem.current.currentSelectedGameObject;
            GameObject currName = InvSelection.transform.parent.Find("ItemName").gameObject;
            String currNameText = currName.GetComponent<Text>().text;

            currentItem = items[currNameText];
            isInvSelected = true;
            BuyButton.SetActive(false);
            SellButton.SetActive(true);
        }

        public void OnItemClick()
        {
            if(itemLoader == null)
            {
                itemLoader = GetComponent<ItemLoader>();
                items = itemLoader.InitItems();
            }

            if(items.Count == 0)
            {
                items = itemLoader.InitItems();
            }

            if (!BuyButton.activeSelf)
            {
                SellButton.SetActive(false);
                BuyButton.SetActive(true);
            }
            GameObject currSelection = EventSystem.current.currentSelectedGameObject;
            GameObject currName = currSelection.transform.parent.Find("ItemName").gameObject;
            String currNameText = currName.GetComponent<Text>().text;
            currentItem = items[currNameText];

            isInvSelected = false;
            InvSelection = null;
            switch (currentItem.Tier) {
                case 1:
                    if (T3CollumnPanel.activeSelf) {
                        T3CollumnPanel.SetActive(false);
                    }
                    foreach(Transform child in T2CollumnPanel.transform)
                    {
                        Destroy(child.gameObject);
                    }
                    foreach(Transform child in T3CollumnPanel.transform)
                    {
                        Destroy(child.gameObject);
                    }
                    for(int i = 0; i <= currentItem.BuildsInto.Length - 1; i++)
                    {
                        if (currentItem.BuildsInto[i] == null)
                            continue;
                        GameObject tempPanel = Instantiate(ItemPanel, T2CollumnPanel.transform);
                        tempPanel.SetActive(true);
                        GameObject ItemName = tempPanel.transform.Find("ItemName").gameObject;
                        ItemName.GetComponent<Text>().text = currentItem.BuildsInto[i].Name;
                        GameObject ItemIcon = tempPanel.transform.Find("ItemIcon").gameObject;
                        ItemIcon.GetComponent<Image>().overrideSprite = currentItem.BuildsInto[i].icon;
                        /*
                        if(Resources.Load<Sprite>("Items/ItemIcons/" + items[currentItem.Child[i]].Name) != null)
                        {
                            ItemIcon.GetComponent<Image>().overrideSprite =
                            Resources.Load<Sprite>("Items/ItemIcons/" + items[currentItem.Child[i]].Name);
                        }
                        */

                        GameObject ItemCost = tempPanel.transform.Find("ItemCost").gameObject;
                        if(Player.ControlForger.items.Contains(currentItem))
                        {
                            ItemCost.GetComponent<Text>().text =
                                (currentItem.BuildsInto[i].Cost - currentItem.Cost).ToString();
                                //(items[currentItem.Child[i]].Cost - currentItem.Cost).ToString();
                        }
                        else if(!Player.ControlForger.items.Contains(currentItem))
                        {
                            ItemCost.GetComponent<Text>().text =
                                currentItem.BuildsInto[i].Cost.ToString();
                                //items[currentItem.Child[i]].Cost.ToString();    
                        }
                        GameObject ItemButton = tempPanel.transform.Find("ItemButton").gameObject;
                        ItemButton.GetComponent<Button>().onClick.AddListener(OnItemClick);
                    }
                    T2CollumnPanel.SetActive(true);
                    SelectionCostText.GetComponent<Text>().text = currentItem.Cost.ToString();
                    break;
                case 2:
                    foreach(Transform child in T3CollumnPanel.transform)
                    {
                        Destroy(child.gameObject);
                    }
                    for(int i = 0; i <= currentItem.BuildsInto.Length - 1; i++)
                    {
                        GameObject tempPanel = Instantiate(ItemPanel, T3CollumnPanel.transform);
                        tempPanel.SetActive(true);
                        GameObject ItemName = tempPanel.transform.Find("ItemName").gameObject;
                        ItemName.GetComponent<Text>().text = currentItem.BuildsInto[i].Name;
                        GameObject ItemIcon = tempPanel.transform.Find("ItemIcon").gameObject;
                        ItemIcon.GetComponent<Image>().overrideSprite =
                                    currentItem.BuildsInto[i].icon;
                        GameObject ItemCost = tempPanel.transform.Find("ItemCost").gameObject;
                        if(currentItem.BuildsFrom.Length > 1)
                        {
                            if(Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]) || Player.ControlForger.items.Contains(currentItem.BuildsFrom[1]))
                            {
                                if(Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text = 
                                        (currentItem.BuildsInto[i].Cost - currentItem.Cost).ToString();
                                }
                                else if(!Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text = 
                                        (currentItem.BuildsInto[i].Cost - currentItem.BuildsFrom[0].Cost).ToString();
                                }
                            }
                            else if(!Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]) && !Player.ControlForger.items.Contains(currentItem.BuildsFrom[1]))
                            {
                                if(Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text =
                                                (currentItem.BuildsInto[i].Cost - currentItem.Cost).ToString();
                                }
                                else if(!Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text = 
                                        currentItem.BuildsInto[i].Cost.ToString();
                                }
                            }
                        }
                        else 
                        {
                            if(Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]))
                            {
                                if(Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text = 
                                        (currentItem.BuildsInto[i].Cost - currentItem.Cost).ToString();
                                }
                                else if(!Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text =
                                        (currentItem.BuildsInto[i].Cost - currentItem.BuildsFrom[0].Cost).ToString();
                                }
                            }
                            else if(!Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]))
                            {
                                if(Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text =
                                                (currentItem.BuildsInto[i].Cost - currentItem.Cost).ToString();
                                }
                                else if(!Player.ControlForger.items.Contains(currentItem))
                                {
                                    ItemCost.GetComponent<Text>().text = 
                                        currentItem.BuildsInto[i].Cost.ToString();
                                }
                            }
                        }
                        GameObject ItemButton = tempPanel.transform.Find("ItemButton").gameObject;
                        ItemButton.GetComponent<Button>().onClick.AddListener(OnItemClick);
                    }
                    T3CollumnPanel.SetActive(true);
                    if(currentItem.BuildsFrom.Length > 1)
                    {
                        if (Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]) || Player.ControlForger.items.Contains(currentItem.BuildsFrom[1]))
                        {
                            SelectionCostText.GetComponent<Text>().text =
                                (currentItem.Cost - currentItem.BuildsFrom[0].Cost).ToString();
                        }
                        else
                        {
                            SelectionCostText.GetComponent<Text>().text = currentItem.Cost.ToString();
                        }
                    }
                    else if(Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]))
                    {
                        SelectionCostText.GetComponent<Text>().text =
                            (currentItem.Cost - currentItem.BuildsFrom[0].Cost).ToString();
                    }
                    else
                    {
                        SelectionCostText.GetComponent<Text>().text = currentItem.Cost.ToString();
                    }
                    break;
                case 3:
                    if(Player.ControlForger.items.Contains(currentItem.BuildsFrom[0]))
                    {
                        SelectionCostText.GetComponent<Text>().text = (currentItem.Cost - currentItem.BuildsFrom[0].Cost).ToString();
                    }
                    else if(currentItem.BuildsFrom[0].BuildsFrom.Length > 1)
                    {
                        if(Player.ControlForger.items.Contains(currentItem.BuildsFrom[0].BuildsFrom[0]) || 
                            Player.ControlForger.items.Contains(currentItem.BuildsFrom[0].BuildsFrom[1]))
                        {
                            SelectionCostText.GetComponent<Text>().text = 
                                (currentItem.Cost - currentItem.BuildsFrom[0].BuildsFrom[0].Cost).ToString();
                        }
                    }
                    else if(currentItem.BuildsFrom[0].BuildsFrom.Length <= 1)
                    {
                        if(Player.ControlForger.items.Contains(currentItem.BuildsFrom[0].BuildsFrom[0]))
                        {
                            SelectionCostText.GetComponent<Text>().text = 
                                (currentItem.Cost - currentItem.BuildsFrom[0].BuildsFrom[0].Cost).ToString();
                        }
                    }
                    else
                    {
                        SelectionCostText.GetComponent<Text>().text = currentItem.Cost.ToString();
                    }
                    break;
            }
            if(Resources.Load<Sprite>("Items/ItemIcons/" + currentItem.Name) != null){
                SelectionImage.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Items/ItemIcons/" + currentItem.Name);
            }
            else{
                SelectionImage.GetComponent<Image>().overrideSprite = Resources.Load<Sprite>("Images/Portraits/Ainsley128");
            }
            SelectionNameText.GetComponent<Text>().text = currentItem.Name;
            SelectionStatsText.GetComponent<Text>().text = "";
            List<StatType> currentStats = new List<StatType>();
            foreach(StatType stat in currentItem.ListStats)
            {
                var statValue = currentItem.StaticItemStats.GetStatValue(stat);
                SelectionStatsText.GetComponent<Text>().text += string.Format("+ {0} {1}\n", statValue, stat);
                /*
                System.Object statValue = currentItem.ItemStats(null);
                FieldInfo StatField = currentItem.ItemStats(null).GetType().GetField(stat.ToString());
                SelectionStatsText.GetComponent<Text>().text += ("+ " + StatField.GetValue(statValue) + " " + stat.ToString() + "\n");
                */
            }

            SelectionPassiveText.GetComponent<Text>().text = currentItem.PassiveDesc; 
        }

        public void BuyItem()
        {
            Vector3 playerSpawn = GameManager.Instance.GetMySpawn(Player.ControlForger);
            float buyDistance =  800f;
            buyDistance *= Constants.TO_UNITY_UNITS;
            Collider[] cols = Physics.OverlapSphere(playerSpawn, buyDistance);
            if( cols.Contains(Player.ControlForger.gameObject.GetComponent<Collider>()) )
            {
                Debug.Log("CLIENT : Trying to buy item");
                if (currentItem == null)
                {
                    return;
                }
                if (Player.ControlForger.Kind == UnitKind.Forger)
                {
                    if (Player.BuyItem(currentItem)) {
                        // @VigorouslySalted Moved your code to a separate method -~ Mox
                    }
                }
            }
        }

        /// <summary>
        /// Redraws the items listed in itemNames.
        /// </summary>
        public void RedrawItems () {
            {
                {
                    {   // Preserving indentation for now to preserve authorship.
                        int i = 0;
                        foreach(string itemname in Player.ControlForger.itemNames)
                        {
                            if(itemname != null)
                            {
                                GameObject ItemIcon = InventorySlots[i].transform.Find("InventorySlotButton").gameObject;
                                if(Resources.Load<Sprite>("Items/ItemIcons/" + itemname) != null){
                                    ItemIcon.GetComponent<Image>().overrideSprite =
                                        Resources.Load<Sprite>("Items/ItemIcons/" + itemname);
                                }
                                else{
                                    ItemIcon.GetComponent<Image>().overrideSprite = 
                                        Resources.Load<Sprite>("Images/Portraits/Ainsley128");
                                }

                                GameObject ItemName = InventorySlots[i].transform.Find("ItemName").gameObject;
                                ItemName.GetComponent<Text>().text = itemname;
                        
                                if(Resources.Load<Sprite>("Items/ItemIcons/" + itemname) != null){
                                    BottomItemSlots[i].GetComponent<Image>().overrideSprite = 
                                        Resources.Load<Sprite>("Items/ItemIcons/" + itemname);
                                }
                                else{
                                    BottomItemSlots[i].GetComponent<Image>().overrideSprite = 
                                        Resources.Load<Sprite>("Images/Portraits/Ainsley128"); 
                                }

                                i++;
                            }
                        }
                        for (; i < 6; i++)
                        {
                            GameObject SlotIcon = InventorySlots[i].transform.Find("InventorySlotButton").gameObject;
                            SlotIcon.GetComponent<Image>().overrideSprite =
                                Resources.Load<Sprite>("UI/Shop/ItemBox");
                            GameObject SlotName = InventorySlots[i].transform.Find("ItemName").gameObject;
                            SlotName.GetComponent<Text>().text = null;
                            BottomItemSlots[i].GetComponent<Image>().overrideSprite =
                                Resources.Load<Sprite>("UI/Shop/ItemBox");
                        }
                        if(currentItem != null)
                        if(currentItem.Tier == 1)
                        {
                            //foreach(Transform child in T2CollumnPanel.transform)
                            for (int childIndex = 0; childIndex < currentItem.BuildsInto.Length; childIndex++)
                            {
                                UIPanel child = new UIPanel();
                                child.panel = T2CollumnPanel.transform.GetChild(childIndex).gameObject;
                                Text childNameText = child.Find<Text>("ItemName");
                                Item childItem = currentItem.BuildsInto[childIndex];
                                if(childItem.Name == childNameText.text)
                                {
                                    Text ItemCostText = child.Find<Text>("ItemCost");
                                    ItemCostText.text =
                                        (childItem.Cost - currentItem.Cost).ToString();
                                }
                            }
                        }
                        if(currentItem != null)
                        if(currentItem.Tier == 2)
                        {
                            //foreach(Transform child in T3CollumnPanel.transform)
                            for (int childIndex = 0; childIndex < currentItem.BuildsInto.Length; childIndex++)
                            {
                                UIPanel child = new UIPanel();
                                child.panel = T3CollumnPanel.transform.GetChild(childIndex).gameObject;
                                Text childNameText = child.Find<Text>("ItemName");
                                Item childItem = currentItem.BuildsInto[childIndex];
                                if (childItem.Name == childNameText.text)
                                {
                                    Text ItemCostText = child.Find<Text>("ItemCost");
                                    ItemCostText.text =
                                        (childItem.Cost - currentItem.Cost).ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SellItem()
        {
            Vector3 playerSpawn = GameManager.Instance.GetMySpawn(Player.ControlForger);
            float buyDistance =  800f;
            buyDistance *= Constants.TO_UNITY_UNITS;
            Collider[] cols = Physics.OverlapSphere(playerSpawn, buyDistance);
            if( cols.Contains(Player.ControlForger.gameObject.GetComponent<Collider>()) ) 
            {
                if (currentItem == null)
                {
                    return;
                }
                if (Player.ControlForger.Kind == UnitKind.Forger)
                {
                    Player.SellItem(currentItem);
                    SellButton.SetActive(false);
                    BuyButton.SetActive(true);
                    currentItem = null;
                }
            }
        }

        private void DisableAllPanels() {
            currentItem = null;
            InvSelection = null;
            isInvSelected = false;
            TreePanel.SetActive(false);
            ListPanel.SetActive(false);
        }

        public void OpenTreePanel() {
            this.DisableAllPanels();
            if(T2CollumnPanel.transform.childCount > 0)
            {
                for (int i = 0; i <= T2CollumnPanel.transform.childCount - 1; i++)
                {
                    Destroy(T2CollumnPanel.transform.GetChild(i).gameObject);
                }
            }
            if(T3CollumnPanel.transform.childCount > 0)
            {
                for (int i = 0; i <= T3CollumnPanel.transform.childCount - 1; i++)
                {
                    Destroy(T3CollumnPanel.transform.GetChild(i).gameObject);
                }
            }
            TreePanel.SetActive(true);
 
        }

        public void OpenListPanel() {
            this.DisableAllPanels();
            ListPanel.SetActive(true);
        }

        public void CloseShop() {
            UIManager.Instance.ToggleShopMenu();
        }

        public void FixedUpdate()
        {
            if (Player != null)
            {
                int currentSalt = (int)Mathf.Floor(Player.ControlForger.Salt);
                TopSaltText.GetComponent<Text>().text = currentSalt.ToString();
                BottomSaltText.GetComponent<Text>().text = currentSalt.ToString();
            }
        }

        public void Cheat()
        {
            Player.CmdMoneyCheat_DEBUG();
        }

    }
}
