﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ui 
{
    public class UISlideStates : MonoBehaviour {
        float speed = 5f;
        [SerializeField]
        private Vector2[] slideStates;
        [SerializeField]
        private int whichState;
        [SerializeField]
        bool saveState = false;
        [SerializeField]
        private bool sliding;
        RectTransform tyranosaurus;

    	// Use this for initialization
    	void Start () {
            tyranosaurus = GetComponent<RectTransform>();
            whichState = PlayerPrefs.GetInt(string.Format("{0}.SlideStateUI", name), whichState);
    	}
    	
    	// Update is called once per frame
    	void Update () {
            if (sliding && slideStates.Length > 0) {
                    
                if (Mathf.Approximately( ((Vector2)tyranosaurus.localPosition - slideStates [whichState]).sqrMagnitude, 0f ) ) {
                    tyranosaurus.localPosition = slideStates [whichState];
                    sliding = false;
                }

                tyranosaurus.localPosition = Vector2.Lerp(tyranosaurus.localPosition, slideStates [whichState], speed * Time.deltaTime);

            }
    	}

        public void SlideToNextState () {
            sliding = true;
            ++whichState;
            whichState %= slideStates.Length;
            if (saveState)
            {
                PlayerPrefs.SetInt(string.Format("{0}.SlideStateUI", name), whichState);
            }
        }

        public void Set(int state) {
            whichState = state;
            sliding = true;
            whichState %= slideStates.Length;
            if (saveState)
            {
                PlayerPrefs.SetInt(string.Format("{0}.SlideStateUI", name), whichState);
            }
        }

    }
}