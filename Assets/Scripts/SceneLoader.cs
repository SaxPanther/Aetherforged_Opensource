﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    
    public string[] these;


	// Use this for initialization
	void Start () {
        var loaderScene = SceneManager.GetActiveScene();
        foreach (string name in these) {
            SceneManager.LoadSceneAsync(name,LoadSceneMode.Additive);
        }

        var lastScene = SceneManager.GetSceneAt(these.Length - 1);
        SceneManager.SetActiveScene(lastScene);

        SceneManager.UnloadSceneAsync(loaderScene);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
