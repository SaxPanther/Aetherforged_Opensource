﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/AbilityIndicator" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader 
	{
	Tags { "Queue"="Geometry+1" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha OneMinusSrcAlpha
	ColorMask RGB

	Cull Off Lighting Off ZWrite Off Ztest Off

	pass
	{
	CGPROGRAM

	#pragma vertex vertexFunction
	#pragma fragment fragmentFunction

	#include "UnityCG.cginc"

	//get info from the model
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv :TEXCOORD0;
	};

	struct v2f
	{
		float4 position : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	//bring properties
	float4 _Color;
	sampler2D _MainTex;

	//set out the vertices
	v2f vertexFunction(appdata IN)
	{
		v2f OUT;

		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.uv = IN.uv;

		return OUT;
	}

	//color inside vertices
	fixed4 fragmentFunction(v2f IN) : SV_Target
	{
		float4 textureColor = tex2D( _MainTex, IN.uv);
		_Color.a=0.5;
		return textureColor * _Color;
	}
	ENDCG
	}

	}
}
